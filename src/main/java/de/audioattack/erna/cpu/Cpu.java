/*
 * SPDX-FileCopyrightText: 2020 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu;

/**
 * Describes methods for implementation of MOS 65xx CPUs.
 */
public interface Cpu {

    /**
     * Memory location of lo byte of NMI vector.
     */
    AddressVector NMI_LO = new AddressVectorImpl((byte) 0xFA, (byte) 0xFF);

    /**
     * Memory location of hi byte of NMI vector.
     */
    AddressVector NMI_HI = new AddressVectorImpl((byte) 0xFB, (byte) 0xFF);

    /**
     * Memory location of lo byte of Reset vector.
     */
    AddressVector RESET_LO = new AddressVectorImpl((byte) 0xFC, (byte) 0xFF);

    /**
     * Memory location of lo byte of Reset vector.
     */
    AddressVector RESET_HI = new AddressVectorImpl((byte) 0xFD, (byte) 0xFF);

    /**
     * Memory location of lo byte of IRQ vector.
     */
    AddressVector IRQ_LO = new AddressVectorImpl((byte) 0xFE, (byte) 0xFF);

    /**
     * Memory location of lo byte of IRQ vector.
     */
    AddressVector IRQ_HI = new AddressVectorImpl((byte) 0xFF, (byte) 0xFF);

    /**
     * Resets the CPU.
     */
    void reset();

    /**
     * Triggers an interrupt.
     */
    void requestIrq();

    /**
     * Triggers a non-maskable interrupt.
     */
    void requestNmi();

    /**
     * Gets current position of program counter. This is not necessarily the program counter itself,
     * only the current value.
     *
     * @return current position of the program counter
     */
    AddressVector getCurrentPc();

    /**
     * Gets current position of stack pointer. This is not necessarily the stack pointer itself,
     * only the current value.
     *
     * @return current position of the stack pointer
     */
    AddressVector getStackPointer();

    /**
     * Gets current value of accumulator.
     *
     * @return current value of accumulator
     */
    byte getA();

    /**
     * Sets value of accumulator.
     *
     * @param newValue value to set
     */
    void setA(byte newValue);

    /**
     * Gets value of x-register.
     *
     * @return current value of x-register
     */
    byte getX();

    /**
     * Sets value of x-register.
     *
     * @param newValue value to set
     */
    void setX(byte newValue);

    /**
     * Gets value of y-register.
     *
     * @return current value of x-register
     */
    byte getY();

    /**
     * Sets value of y-register.
     *
     * @param newValue value to set
     */
    void setY(byte newValue);

    /**
     * Gets value of memory cell.
     *
     * @param lo low byte of memory cell
     * @param hi high byte of memory cell
     * @return current value of memory cell
     */
    byte getMemory(byte lo, byte hi);

    /**
     * Gets value of memory cell.
     *
     * @param memAddress address of the memory cell
     * @return current value of memory cell
     */
    byte getMemory(AddressVector memAddress);

    /**
     * Sets value of memory cell.
     *
     * @param lo       low byte of memory cell
     * @param hi       high byte of memory cell
     * @param newValue value to set
     */
    void setMemory(byte lo, byte hi, byte newValue);

    /**
     * Sets value of memory cell.
     *
     * @param memAddress address of the memory cell
     * @param newValue   value to set
     */
    void setMemory(AddressVector memAddress, byte newValue);

    /**
     * Advances the CPU by one tick.
     */
    void tick();

    /**
     * Gets current status flags.
     *
     * @return current status flags
     */
    StatusRegister getCurrentStatus();

    /**
     * Sets status flags.
     *
     * @param status new status
     */
    void setStatus(byte status);
}
