/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * Classes which are related to different address modes of CPU commands.
 */
package de.audioattack.erna.cpu.commands.addressing;
