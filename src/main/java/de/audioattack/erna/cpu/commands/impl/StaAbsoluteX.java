/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page STA command. The STA command copies a value of the accumulator to given absolute X-indexed address.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#STA">
 * http://www.6502.org/tutorials/6502opcodes.html#STA</a>
 */
public class StaAbsoluteX extends StaBase {

    private static final byte OPCODE = (byte) 0x9D;

    private static final byte COMMAND_LENGTH = (byte) 3;

    private static final byte CYCLES = 5;

    /**
     * Constructor.
     */
    public StaAbsoluteX() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.X_INDEXED_ABSOLUTE, CYCLES);
    }

}
