/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.addressing;

import de.audioattack.erna.cpu.AddressVectorImpl;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;

/**
 * Calculates an indirect address.
 */
class AbsoluteIndirectAddressing implements Addressing {

    @Override
    public AddressResult getAddress(final Cpu cpu, final ProgramCounter programCounter) {
        programCounter.increment();
        final byte pointerLo = cpu.getMemory(programCounter);
        programCounter.increment();
        final byte pointerHi = cpu.getMemory(programCounter);

        final byte lo = cpu.getMemory(pointerLo, pointerHi);
        final byte hi = cpu.getMemory((byte) (pointerLo + 1), pointerHi);

        return new AddressResult(new AddressVectorImpl(lo, hi), false);
    }
}
