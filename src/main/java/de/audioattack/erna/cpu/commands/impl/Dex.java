/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.AbstractBaseCommand;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;
import de.audioattack.erna.util.ByteUtil;

/**
 * DEX command. The DEX command decrements the X register.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#DEX">
 * http://www.6502.org/tutorials/6502opcodes.html#DEX</a>
 */
public class Dex extends AbstractBaseCommand {

    private static final byte OPCODE = (byte) 0xCA;

    private static final byte COMMAND_LENGTH = OPCODE;

    private static final byte CYCLES = 2;

    /**
     * Constructor.
     */
    public Dex() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.IMPLIED);
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {
        cpu.setX((byte) (cpu.getX() - 1));
        statusRegister.setNegative(ByteUtil.isNegative(cpu.getX()));
        statusRegister.setZero(cpu.getX() == 0);
        programCounter.increment();
        return CYCLES;
    }
}
