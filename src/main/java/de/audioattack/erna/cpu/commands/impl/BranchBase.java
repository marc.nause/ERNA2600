/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.AddressVector;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.AbstractBaseCommand;
import de.audioattack.erna.cpu.commands.addressing.AddressResult;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;

import java.util.function.Function;

/**
 * Base class for branch commands.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#BCC">
 * http://www.6502.org/tutorials/6502opcodes.html#BCC</a>
 */
class BranchBase extends AbstractBaseCommand {

    private static final byte COMMAND_LENGTH = (byte) 2;

    private static final byte CYCLES = 2;

    private final Function<StatusRegister, Boolean> condition;

    /**
     * Constructor.
     *
     * @param opcode    opcode of command
     * @param condition condition to check for branch
     */
    protected BranchBase(final byte opcode, final Function<StatusRegister, Boolean> condition) {
        super(opcode, COMMAND_LENGTH, AddressingType.RELATIVE);
        this.condition = condition;
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {

        // Implementation of relative addressing also sets program counter to next command
        final AddressResult result = getAddress(cpu, programCounter);

        byte cycles = CYCLES;
        if (condition.apply(statusRegister)) {
            cycles++;
            final AddressVector address = result.getAddress();
            if (programCounter.getHi() != address.getHi()) {
                cycles++;
            }
            programCounter.set(address);
        }

        return cycles;
    }
}
