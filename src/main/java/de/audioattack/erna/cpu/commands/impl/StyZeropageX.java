/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page X-indexed STY command. The STY command copies a value of the Y register to given zero-page address plus X.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#STY">
 * http://www.6502.org/tutorials/6502opcodes.html#STY</a>
 */
public class StyZeropageX extends StyBase {

    private static final byte OPCODE = (byte) 0x94;

    private static final byte COMMAND_LENGTH = (byte) 2;

    private static final byte CYCLES = 4;

    /**
     * Constructor.
     */
    public StyZeropageX() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.X_INDEXED_ZEROPAGE, CYCLES);
    }

}
