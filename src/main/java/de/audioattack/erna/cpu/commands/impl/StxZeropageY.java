/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page Y-indexed STX command. The STX command copies a value of the X register to given zero-page address plus Y.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#STX">
 * http://www.6502.org/tutorials/6502opcodes.html#STX</a>
 */
public class StxZeropageY extends StxBase {

    private static final byte OPCODE = (byte) 0x96;

    private static final byte COMMAND_LENGTH = (byte) 2;

    private static final byte CYCLES = 4;

    /**
     * Constructor.
     */
    public StxZeropageY() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.Y_INDEXED_ZEROPAGE, CYCLES);
    }

}
