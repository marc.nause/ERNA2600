/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Absolute STY command. The STY command copies a value of the Y register to given memory address.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#STY">
 * http://www.6502.org/tutorials/6502opcodes.html#STY</a>
 */
public class StyAbsolute extends StyBase {

    private static final byte OPCODE = (byte) 0x8C;

    private static final byte COMMAND_LENGTH = (byte) 3;

    private static final byte CYCLES = 4;

    /**
     * Constructor.
     */
    public StyAbsolute() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ABSOLUTE, CYCLES);
    }

}
