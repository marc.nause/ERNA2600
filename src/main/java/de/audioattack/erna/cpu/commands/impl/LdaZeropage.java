/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page LDA command. The LDA command copies a value from memory to the accumulator.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#LDA">
 * http://www.6502.org/tutorials/6502opcodes.html#LDA</a>
 */
public class LdaZeropage extends LdaBase {

    private static final byte OPCODE = (byte) 0xA5;

    private static final byte COMMAND_LENGTH = 2;

    private static final byte CYCLES = 3;

    /**
     * Constructor.
     */
    public LdaZeropage() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ZEROPAGE, CYCLES);
    }

}
