/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.AddressVector;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.AbstractBaseCommand;
import de.audioattack.erna.cpu.commands.addressing.AddressResult;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;
import de.audioattack.erna.util.ByteUtil;

import java.util.function.BiConsumer;

/**
 * Base class for LDA, LDY, and LDX command implementations.
 */
class LoadBase extends AbstractBaseCommand {

    private final byte cycles;

    private final BiConsumer<Cpu, Byte> registerFunction;

    /**
     * Constructor.
     *
     * @param opcode           opcode of command
     * @param numberOfBytes    length of command in bytes
     * @param addressingType   addressing type of command
     * @param cycles           duration of command in CPU cycles
     * @param registerFunction function to set register
     */
    protected LoadBase(
            final byte opcode,
            final byte numberOfBytes,
            final AddressingType addressingType,
            final byte cycles,
            final BiConsumer<Cpu, Byte> registerFunction) {
        super(opcode, numberOfBytes, addressingType);

        this.registerFunction = registerFunction;
        this.cycles = cycles;
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {

        final AddressResult result = getAddress(cpu, programCounter);
        final AddressVector address = result.getAddress();

        setPageBoundaryCrossed(programCounter, address);

        final byte b = cpu.getMemory(address);
        registerFunction.accept(cpu, b);

        statusRegister.setNegative(ByteUtil.isNegative(b));
        statusRegister.setZero(b == 0);

        return cycles;
    }
}
