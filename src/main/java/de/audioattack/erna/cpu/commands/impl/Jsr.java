/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.AbstractBaseCommand;
import de.audioattack.erna.cpu.commands.addressing.AddressResult;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * JSR command. The JSR command pushes the address-1 of the next operation on to the stack before
 * transferring program control to the following address.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#JSR">
 * http://www.6502.org/tutorials/6502opcodes.html#JSR</a>
 */
public class Jsr extends AbstractBaseCommand {

    private static final byte OPCODE = (byte) 0x20;

    private static final byte COMMAND_LENGTH = (byte) 3;

    private static final byte CYCLES = 6;

    /**
     * Constructor.
     */
    public Jsr() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ABSOLUTE);
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {

        final AddressResult addressResult = getAddress(cpu, programCounter);

        cpu.setMemory(stackPointer, programCounter.getHi());
        stackPointer.decrement();
        cpu.setMemory(stackPointer, programCounter.getLo());
        stackPointer.decrement();

        programCounter.set(addressResult.getAddress());

        return CYCLES;
    }
}
