/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Absolute X-indexed LDY command. The LDY command copies a value from memory to the Y-register.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#LDY">
 * http://www.6502.org/tutorials/6502opcodes.html#LDY</a>
 */
public class LdyAbsoluteX extends LdyBase {

    private static final byte OPCODE = (byte) 0xBC;

    private static final byte COMMAND_LENGTH = 3;

    private static final byte CYCLES = 4;

    /**
     * Constructor.
     */
    public LdyAbsoluteX() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.X_INDEXED_ABSOLUTE, CYCLES);
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {
        byte cycles = super.run(cpu, statusRegister, programCounter, stackPointer);
        if (isPageBoundaryCrossed()) {
            cycles++;
        }
        return cycles;
    }
}
