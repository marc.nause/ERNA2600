/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands;

import de.audioattack.erna.cpu.AddressVector;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.commands.addressing.AddressResult;
import de.audioattack.erna.cpu.commands.addressing.Addressing;
import de.audioattack.erna.cpu.commands.addressing.AddressingMap;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Base class for commands.
 */
public abstract class AbstractBaseCommand implements Command {

    private final byte opcode;

    private final byte numberOfBytes;

    private final AddressingType addressingType;

    private final Addressing addressing;

    private Boolean isPageBoundaryCrossed;

    /**
     * Constructor.
     *
     * @param opcode         opcode of the command
     * @param numberOfBytes  number of bytes of the command (includes opcode)
     * @param addressingType addressing type of the command
     */
    protected AbstractBaseCommand(final byte opcode, final byte numberOfBytes, final AddressingType addressingType) {
        this.opcode = opcode;
        this.numberOfBytes = numberOfBytes;
        this.addressingType = addressingType;
        this.addressing = AddressingMap.get(addressingType);
    }

    /**
     * Gets opcode of the command.
     *
     * @return the opcode
     */
    public byte getOpcode() {
        return opcode;
    }

    /**
     * Gets number of bytes of the command (includes opcode).
     *
     * @return the number of bytes
     */
    public byte getNumberOfBytes() {
        return numberOfBytes;
    }

    /**
     * Gets addressing type of the command.
     *
     * @return the addressing type
     */
    public AddressingType getAddressing() {
        return addressingType;
    }

    /**
     * Gets the address to which the command refers.
     *
     * @param cpu            CPU the command runs on
     * @param programCounter command counter of teh CPU
     * @return the address
     */
    protected AddressResult getAddress(final Cpu cpu, final ProgramCounter programCounter) {
        return addressing.getAddress(cpu, programCounter);
    }

    /**
     * Sets value which tells if page boundary was crossed. Page boundary was crossed if both addresses are located in
     * different pages of the memory, which is the case if their high bytes are different.
     *
     * @param firstAddress  first address to compare
     * @param secnodAddress second address to compare
     */
    protected void setPageBoundaryCrossed(final AddressVector firstAddress, final AddressVector secnodAddress) {
        isPageBoundaryCrossed = firstAddress.getHi() != secnodAddress.getHi();
    }

    /**
     * Tells if page boundary was crossed.
     *
     * @return {@code true} if page boundary was crossed, else {@code false}
     */
    protected boolean isPageBoundaryCrossed() {
        if (isPageBoundaryCrossed == null) {
            throw new IllegalStateException("Value 'isPageBoundaryCrossed' not set");
        }

        return isPageBoundaryCrossed;
    }
}
