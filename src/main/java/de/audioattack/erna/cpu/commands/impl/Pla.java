/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.AbstractBaseCommand;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;
import de.audioattack.erna.util.ByteUtil;

/**
 * PLA command. The PLA command pulls a value from the stack and puts it into the accumulator.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#PLA">
 * http://www.6502.org/tutorials/6502opcodes.html#PLA</a>
 */
public class Pla extends AbstractBaseCommand {

    private static final byte OPCODE = (byte) 0x68;

    private static final byte COMMAND_LENGTH = (byte) 1;

    private static final byte CYCLES = 4;

    /**
     * Constructor.
     */
    public Pla() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.IMPLIED);
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {
        stackPointer.increment();
        cpu.setA(cpu.getMemory(stackPointer));
        statusRegister.setNegative(ByteUtil.isNegative(cpu.getA()));
        statusRegister.setZero(cpu.getA() == 0);
        programCounter.increment();
        return CYCLES;
    }
}
