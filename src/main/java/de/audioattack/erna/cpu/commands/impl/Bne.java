/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

/**
 * BNE command. The BNE command branches if zero flag is not set.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#BNE">
 * http://www.6502.org/tutorials/6502opcodes.html#BNE</a>
 */
public class Bne extends BranchBase {

    private static final byte OPCODE = (byte) 0xD0;

    /**
     * Constructor.
     */
    public Bne() {
        super(OPCODE, statusRegister -> !statusRegister.isZeroSet());
    }
}
