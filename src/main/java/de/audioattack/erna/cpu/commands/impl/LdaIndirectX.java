/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Indirect X-indexed LDA command. The LDA command copies a value from memory to the accumulator.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#LDA">
 * http://www.6502.org/tutorials/6502opcodes.html#LDA</a>
 */
public class LdaIndirectX extends LdaBase {

    private static final byte OPCODE = (byte) 0xA1;

    private static final byte COMMAND_LENGTH = 2;

    private static final byte CYCLES = 6;

    /**
     * Constructor.
     */
    public LdaIndirectX() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.X_INDEXED_ZEROPAGE_INDIRECT, CYCLES);
    }
    
}
