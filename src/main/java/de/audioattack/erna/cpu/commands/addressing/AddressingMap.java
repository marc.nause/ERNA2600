/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.addressing;

import java.util.EnumMap;

/**
 * Contains calculators for addresses of the different addressing modes.
 */
@SuppressWarnings({"ClassDataAbstractionCoupling"})
public final class AddressingMap {

    private static final EnumMap<AddressingType, Addressing> ADDRESSING_MAP = new EnumMap<>(AddressingType.class);

    /**
     * Private constructor to avoid instantiation of static utility class.
     */
    private AddressingMap() {
    }

    static {
        ADDRESSING_MAP.put(AddressingType.ABSOLUTE, new AbsoluteAddressing());
        ADDRESSING_MAP.put(AddressingType.ABSOLUTE_INDIRECT, new AbsoluteIndirectAddressing());
        ADDRESSING_MAP.put(AddressingType.ACCUMULATOR, null);
        ADDRESSING_MAP.put(AddressingType.IMMEDIATE, null);
        ADDRESSING_MAP.put(AddressingType.IMPLIED, null);
        ADDRESSING_MAP.put(AddressingType.RELATIVE, new RelativeAddressing());
        ADDRESSING_MAP.put(AddressingType.X_INDEXED_ABSOLUTE, new XIndexedAbsoluteAddressing());
        ADDRESSING_MAP.put(AddressingType.X_INDEXED_ZEROPAGE, new XIndexedZeropageAddressing());
        ADDRESSING_MAP.put(AddressingType.X_INDEXED_ZEROPAGE_INDIRECT, new XIndexedZeropageIndirectAddressing());
        ADDRESSING_MAP.put(AddressingType.Y_INDEXED_ABSOLUTE, new YIndexedAbsoluteAddressing());
        ADDRESSING_MAP.put(AddressingType.Y_INDEXED_ZEROPAGE, new YIndexedZeropageAddressing());
        ADDRESSING_MAP.put(AddressingType.ZEROPAGE, new ZeropageAddressing());
        ADDRESSING_MAP.put(AddressingType.ZEROPAGE_INDIRECT_Y_INDEXED, new ZeropageIndirectYIndexedAddressing());
    }

    /**
     * Get address calculator of a given addressing.
     *
     * @param addressingType the addressing
     * @return the address calculator
     */
    public static Addressing get(final AddressingType addressingType) {
        return ADDRESSING_MAP.get(addressingType);
    }
}
