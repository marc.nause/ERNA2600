/*
 * SPDX-FileCopyrightText: 2023 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.AbstractBaseCommand;
import de.audioattack.erna.cpu.commands.addressing.AddressResult;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Base class for JMP commands.
 */
class JmpBase extends AbstractBaseCommand {

    private final byte cycles;

    /**
     * Constructor.
     *
     * @param opcode         opcode of command
     * @param numberOfBytes  length of command in bytes
     * @param addressingType addressing type of command
     * @param cycles         duration of command in CPU cycles
     */
    protected JmpBase(
            final byte opcode,
            final byte numberOfBytes,
            final AddressingType addressingType,
            final byte cycles) {
        super(opcode, numberOfBytes, addressingType);

        this.cycles = cycles;
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {

        final AddressResult result = getAddress(cpu, programCounter);
        programCounter.set(result.getAddress());

        return cycles;
    }
}
