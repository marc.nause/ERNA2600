/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.addressing;

import de.audioattack.erna.cpu.AddressVectorImpl;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;

/**
 * Calculates an absolute address.
 */
class AbsoluteAddressing implements Addressing {

    @Override
    public AddressResult getAddress(final Cpu cpu, final ProgramCounter programCounter) {
        programCounter.increment();
        final byte lo = cpu.getMemory(programCounter);
        programCounter.increment();
        final byte hi = cpu.getMemory(programCounter);

        return new AddressResult(new AddressVectorImpl(lo, hi), false);
    }
}
