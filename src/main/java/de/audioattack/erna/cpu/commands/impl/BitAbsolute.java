/*
 * SPDX-FileCopyrightText: 2023 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Absolute BIT command. The BIT command does a bitwise compare of the accumulator and an
 * absolutely referenced memory location.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#BIT">
 * http://www.6502.org/tutorials/6502opcodes.html#BIT</a>
 */
public class BitAbsolute extends BitBase {

    private static final byte OPCODE = (byte) 0x2C;

    private static final byte COMMAND_LENGTH = 3;

    private static final byte CYCLES = 4;

    /**
     * Constructor.
     */
    public BitAbsolute() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ABSOLUTE, CYCLES);
    }

}
