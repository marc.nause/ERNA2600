/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

/**
 * BCC command. The BCC command branches if carry is not set.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#BCC">
 * http://www.6502.org/tutorials/6502opcodes.html#BCC</a>
 */
public class Bcc extends BranchBase {

    private static final byte OPCODE = (byte) 0x90;

    /**
     * Constructor.
     */
    public Bcc() {
        super(OPCODE, statusRegister -> !statusRegister.isCarrySet());
    }
}
