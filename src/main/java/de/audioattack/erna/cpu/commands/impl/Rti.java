/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.AddressVectorImpl;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.AbstractBaseCommand;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * RTI command. The RTI command retrieves the status flags and the program counter from the stack.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#RTI">
 * http://www.6502.org/tutorials/6502opcodes.html#RTI</a>
 */
public class Rti extends AbstractBaseCommand {

    private static final byte OPCODE = (byte) 0x40;

    private static final byte COMMAND_LENGTH = (byte) 1;

    private static final byte CYCLES = 6;

    /**
     * Constructor.
     */
    public Rti() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.IMPLIED);
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {


        stackPointer.increment();
        statusRegister.set(cpu.getMemory(stackPointer));

        stackPointer.increment();
        final byte lo = cpu.getMemory(stackPointer);
        stackPointer.increment();
        final byte hi = cpu.getMemory(stackPointer);
        programCounter.set(new AddressVectorImpl(lo, hi));

        return CYCLES;
    }
}
