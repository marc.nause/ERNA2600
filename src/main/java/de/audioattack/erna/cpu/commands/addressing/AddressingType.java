/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.addressing;

/**
 * Addressing modes according to the
 * <a href="https://www.pagetable.com/c64ref/6502/?tab=3">6502 Family CPU Reference</a>.
 */
public enum AddressingType {

    /**
     * The address containing the operand is implicitly stated in the operation code of the instruction.
     */
    IMPLIED,

    /**
     * A - Accumulator holds data.
     */
    ACCUMULATOR,

    /**
     * nn - Location nn holds data.
     */
    ABSOLUTE,

    /**
     * nn, X - Location nn + X holds data.
     */
    X_INDEXED_ABSOLUTE,

    /**
     * nn, Y - Location nn + Y holds data.
     */
    Y_INDEXED_ABSOLUTE,

    /**
     * #n - n is data.
     */
    IMMEDIATE,

    /**
     * (nn) - Location nn and next hold address to jump to.
     */
    ABSOLUTE_INDIRECT,

    /**
     * (n, X) - Location n + X and next of page 0 hold address of data.
     */
    X_INDEXED_ZEROPAGE_INDIRECT,

    /**
     * (n), Y - Location n + Y and next of page 0 hold address of data.
     */
    ZEROPAGE_INDIRECT_Y_INDEXED,

    /**
     * n - Address to jump to is n + address of next instruction, with n treated as a signed number.
     */
    RELATIVE,

    /**
     * n - Location n of page 0 holds data.
     */
    ZEROPAGE,

    /**
     * n, X - Location n + X of page 0 holds data.
     */
    X_INDEXED_ZEROPAGE,

    /**
     * n, Y - Location n + Y of page 0 holds data.
     */
    Y_INDEXED_ZEROPAGE

}
