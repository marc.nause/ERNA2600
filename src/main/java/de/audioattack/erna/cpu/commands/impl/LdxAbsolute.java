/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Absolute LDX command. The LDX command copies a value from memory to the X-register.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#LDX">
 * http://www.6502.org/tutorials/6502opcodes.html#LDX</a>
 */
public class LdxAbsolute extends LdxBase {

    private static final byte OPCODE = (byte) 0xAE;

    private static final byte COMMAND_LENGTH = 3;

    private static final byte CYCLES = 4;

    /**
     * Constructor.
     */
    public LdxAbsolute() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ABSOLUTE, CYCLES);
    }

}
