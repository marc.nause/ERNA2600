/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Absolute Y-indexed LDA command. The LDA command copies a value from memory to the accumulator.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#LDA">
 * http://www.6502.org/tutorials/6502opcodes.html#LDA</a>
 */
public class LdaAbsoluteY extends LdaBase {

    private static final byte OPCODE = (byte) 0xB9;

    private static final byte COMMAND_LENGTH = 3;

    private static final byte CYCLES = 4;

    /**
     * Constructor.
     */
    public LdaAbsoluteY() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.Y_INDEXED_ABSOLUTE, CYCLES);
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {
        byte cycles = super.run(cpu, statusRegister, programCounter, stackPointer);
        if (isPageBoundaryCrossed()) {
            cycles++;
        }
        return cycles;
    }
}
