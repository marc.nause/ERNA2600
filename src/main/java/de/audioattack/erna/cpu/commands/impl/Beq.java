/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.StatusRegister;

/**
 * BEQ command. The BEQ command branches if zero flag is set.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#BEQ">
 * http://www.6502.org/tutorials/6502opcodes.html#BEQ</a>
 */
public class Beq extends BranchBase {

    private static final byte OPCODE = (byte) 0xF0;

    /**
     * Constructor.
     */
    public Beq() {
        super(OPCODE, StatusRegister::isZeroSet);
    }
}
