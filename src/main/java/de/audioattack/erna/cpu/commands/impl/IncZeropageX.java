/*
 * SPDX-FileCopyrightText: 2023 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page X-indexed INC command. The INC increments a zero-page memory location.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#INC">
 * http://www.6502.org/tutorials/6502opcodes.html#INC</a>
 */
public class IncZeropageX extends IncBase {

    private static final byte OPCODE = (byte) 0xF6;

    private static final byte COMMAND_LENGTH = 2;

    private static final byte CYCLES = 7;

    /**
     * Constructor.
     */
    public IncZeropageX() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.X_INDEXED_ZEROPAGE, CYCLES);
    }

}
