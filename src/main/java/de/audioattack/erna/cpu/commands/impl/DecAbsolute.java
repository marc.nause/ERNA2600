/*
 * SPDX-FileCopyrightText: 2023 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page DEC command. The DEC decrements the value in the following memory location.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#DEC">
 * http://www.6502.org/tutorials/6502opcodes.html#DEC</a>
 */
public class DecAbsolute extends DecBase {

    private static final byte OPCODE = (byte) 0xCE;

    private static final byte COMMAND_LENGTH = 3;

    private static final byte CYCLES = 6;

    /**
     * Constructor.
     */
    public DecAbsolute() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ABSOLUTE, CYCLES);
    }

}
