/*
 * SPDX-FileCopyrightText: 2023 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page INC command. The INC increments the value in the following memory location.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#INC">
 * http://www.6502.org/tutorials/6502opcodes.html#INC</a>
 */
public class IncAbsolute extends IncBase {

    private static final byte OPCODE = (byte) 0xEE;

    private static final byte COMMAND_LENGTH = 3;

    private static final byte CYCLES = 6;

    /**
     * Constructor.
     */
    public IncAbsolute() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ABSOLUTE, CYCLES);
    }

}
