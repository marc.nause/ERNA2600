/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.AbstractBaseCommand;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;
import de.audioattack.erna.util.ByteUtil;

/**
 * TAX command. The TAX command transfers the content of the accumulator to the X register.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#TAX">
 * http://www.6502.org/tutorials/6502opcodes.html#TAX</a>
 */
public class Tax extends AbstractBaseCommand {

    private static final byte OPCODE = (byte) 0xAA;

    private static final byte COMMAND_LENGTH = (byte) 1;

    private static final byte CYCLES = 2;

    /**
     * Constructor.
     */
    public Tax() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.IMPLIED);
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {
        cpu.setX(cpu.getA());
        statusRegister.setNegative(ByteUtil.isNegative(cpu.getA()));
        statusRegister.setZero(cpu.getA() == 0);
        programCounter.increment();
        return CYCLES;
    }
}
