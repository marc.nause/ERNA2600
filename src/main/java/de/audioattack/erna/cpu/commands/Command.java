/*
 * SPDX-FileCopyrightText: 2020 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;

/**
 * Methods of a CPU command object.
 */
public interface Command {

    /**
     * Gets opcode of command.
     *
     * @return the opcode
     */
    byte getOpcode();

    /**
     * Gets number of bytes the command consists of, including the opcode.
     *
     * @return length of command
     */
    byte getNumberOfBytes();

    /**
     * Executes the command.
     *
     * @param cpu            CPU of the system
     * @param statusRegister status flags of the system
     * @param programCounter program counter of the system
     * @param stackPointer   stack pointer of the system
     * @return number of cycles the command takes
     */
    byte run(Cpu cpu, StatusRegister statusRegister, ProgramCounter programCounter, StackPointer stackPointer);
}
