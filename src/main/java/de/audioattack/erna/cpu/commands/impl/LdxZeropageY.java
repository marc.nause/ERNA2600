/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page Y-indexed LDX command. The LDX command copies a value from memory to the X-register.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#LDX">
 * http://www.6502.org/tutorials/6502opcodes.html#LDX</a>
 */
public class LdxZeropageY extends LdxBase {

    private static final byte OPCODE = (byte) 0xB6;

    private static final byte COMMAND_LENGTH = 2;

    private static final byte CYCLES = 4;

    /**
     * Constructor.
     */
    public LdxZeropageY() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.Y_INDEXED_ZEROPAGE, CYCLES);
    }

}
