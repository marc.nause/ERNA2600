/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page LDY command. The LDY command copies a value from memory to the Y-register.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#LDY">
 * http://www.6502.org/tutorials/6502opcodes.html#LDY</a>
 */
public class LdyZeropage extends LdyBase {

    private static final byte OPCODE = (byte) 0xA4;

    private static final byte COMMAND_LENGTH = 2;

    private static final byte CYCLES = 3;

    /**
     * Constructor.
     */
    public LdyZeropage() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ZEROPAGE, CYCLES);
    }

}
