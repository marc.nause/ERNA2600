/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

/**
 * BPL command. The BPL command branches if negative flag is not set.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#BPL">
 * http://www.6502.org/tutorials/6502opcodes.html#BPL</a>
 */
public class Bpl extends BranchBase {

    private static final byte OPCODE = (byte) 0x10;

    /**
     * Constructor.
     */
    public Bpl() {
        super(OPCODE, statusRegister -> !statusRegister.isNegativeSet());
    }
}
