/*
 * SPDX-FileCopyrightText: 2023 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.AddressVector;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.AbstractBaseCommand;
import de.audioattack.erna.cpu.commands.addressing.AddressResult;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;
import de.audioattack.erna.util.ByteUtil;

/**
 * Base class for DEC command implementations. The DEC command decrements a memory location.
 */
class DecBase extends AbstractBaseCommand {

    private static final int HALF_BYTE = 0x81;

    private final byte cycles;

    /**
     * Constructor.
     *
     * @param opcode         opcode of command
     * @param numberOfBytes  length of command in bytes
     * @param addressingType addressing type of command
     * @param cycles         duration of command in CPU cycles
     */
    protected DecBase(
            final byte opcode,
            final byte numberOfBytes,
            final AddressingType addressingType,
            final byte cycles) {
        super(opcode, numberOfBytes, addressingType);

        this.cycles = cycles;
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {

        final AddressResult result = getAddress(cpu, programCounter);
        final AddressVector address = result.getAddress();

        final byte valMemory = cpu.getMemory(address);
        final byte newValue = (byte) (valMemory - 1);
        cpu.setMemory(address, newValue);

        statusRegister.setZero(newValue == 0);
        statusRegister.setNegative((newValue & ByteUtil.ALL_BITS_SET) <= HALF_BYTE);

        programCounter.increment();

        return cycles;
    }
}
