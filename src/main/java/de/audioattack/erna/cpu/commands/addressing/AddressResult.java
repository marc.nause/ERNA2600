/*
 * SPDX-FileCopyrightText: 2023 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.addressing;

import de.audioattack.erna.cpu.AddressVector;

/**
 * Result of an address calculation.
 */
public class AddressResult {

    private final AddressVector address;

    private final boolean isPageBoundaryCrossed;

    /**
     * Constructor.
     *
     * @param address               the address
     * @param isPageBoundaryCrossed {@code true} if page boundary is crossed, else {@code false}
     */
    public AddressResult(final AddressVector address, final boolean isPageBoundaryCrossed) {
        this.address = address;
        this.isPageBoundaryCrossed = isPageBoundaryCrossed;
    }

    /**
     * Gets address.
     *
     * @return the address
     */
    public AddressVector getAddress() {
        return address;
    }

    /**
     * Tells if if page boundary is crossed.
     *
     * @return {@code true} if page boundary is crossed, else {@code false}
     */
    public boolean isPageBoundaryCrossed() {
        return isPageBoundaryCrossed;
    }
}
