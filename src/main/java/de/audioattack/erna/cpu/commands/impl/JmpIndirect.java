/*
 * SPDX-FileCopyrightText: 2023 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Indirect JMP command. The JMP command transfers the program execution to the location contained in the following
 * address.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#JMP">
 * http://www.6502.org/tutorials/6502opcodes.html#JMP</a>
 */
public class JmpIndirect extends JmpBase {

    private static final byte OPCODE = (byte) 0x6C;

    private static final byte COMMAND_LENGTH = (byte) 3;

    private static final byte CYCLES = 5;

    /**
     * Constructor.
     */
    public JmpIndirect() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ABSOLUTE_INDIRECT, CYCLES);
    }

}
