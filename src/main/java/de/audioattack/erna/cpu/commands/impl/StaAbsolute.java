/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Absolute STA command. The STA command copies a value of the accumulator to given memory address.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#STA">
 * http://www.6502.org/tutorials/6502opcodes.html#STA</a>
 */
public class StaAbsolute extends StaBase {

    private static final byte OPCODE = (byte) 0x8D;

    private static final byte COMMAND_LENGTH = (byte) 3;

    private static final byte CYCLES = 4;

    /**
     * Constructor.
     */
    public StaAbsolute() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ABSOLUTE, CYCLES);
    }

}
