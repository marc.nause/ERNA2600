/*
 * SPDX-FileCopyrightText: 2020 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands;

import de.audioattack.erna.cpu.commands.impl.Bcc;
import de.audioattack.erna.cpu.commands.impl.Bcs;
import de.audioattack.erna.cpu.commands.impl.Beq;
import de.audioattack.erna.cpu.commands.impl.BitAbsolute;
import de.audioattack.erna.cpu.commands.impl.BitZeropage;
import de.audioattack.erna.cpu.commands.impl.Bmi;
import de.audioattack.erna.cpu.commands.impl.Bne;
import de.audioattack.erna.cpu.commands.impl.Bpl;
import de.audioattack.erna.cpu.commands.impl.Brk;
import de.audioattack.erna.cpu.commands.impl.Bvc;
import de.audioattack.erna.cpu.commands.impl.Bvs;
import de.audioattack.erna.cpu.commands.impl.Clc;
import de.audioattack.erna.cpu.commands.impl.Cld;
import de.audioattack.erna.cpu.commands.impl.Cli;
import de.audioattack.erna.cpu.commands.impl.Clv;
import de.audioattack.erna.cpu.commands.impl.DecAbsolute;
import de.audioattack.erna.cpu.commands.impl.DecAbsoluteX;
import de.audioattack.erna.cpu.commands.impl.DecZeropage;
import de.audioattack.erna.cpu.commands.impl.DecZeropageX;
import de.audioattack.erna.cpu.commands.impl.Dex;
import de.audioattack.erna.cpu.commands.impl.Dey;
import de.audioattack.erna.cpu.commands.impl.IncAbsolute;
import de.audioattack.erna.cpu.commands.impl.IncAbsoluteX;
import de.audioattack.erna.cpu.commands.impl.IncZeropage;
import de.audioattack.erna.cpu.commands.impl.IncZeropageX;
import de.audioattack.erna.cpu.commands.impl.Inx;
import de.audioattack.erna.cpu.commands.impl.Iny;
import de.audioattack.erna.cpu.commands.impl.JmpAbsolute;
import de.audioattack.erna.cpu.commands.impl.JmpIndirect;
import de.audioattack.erna.cpu.commands.impl.Jsr;
import de.audioattack.erna.cpu.commands.impl.LdaAbsolute;
import de.audioattack.erna.cpu.commands.impl.LdaAbsoluteX;
import de.audioattack.erna.cpu.commands.impl.LdaAbsoluteY;
import de.audioattack.erna.cpu.commands.impl.LdaImmediate;
import de.audioattack.erna.cpu.commands.impl.LdaIndirectX;
import de.audioattack.erna.cpu.commands.impl.LdaIndirectY;
import de.audioattack.erna.cpu.commands.impl.LdaZeropage;
import de.audioattack.erna.cpu.commands.impl.LdaZeropageX;
import de.audioattack.erna.cpu.commands.impl.LdxAbsolute;
import de.audioattack.erna.cpu.commands.impl.LdxAbsoluteY;
import de.audioattack.erna.cpu.commands.impl.LdxImmediate;
import de.audioattack.erna.cpu.commands.impl.LdxZeropage;
import de.audioattack.erna.cpu.commands.impl.LdxZeropageY;
import de.audioattack.erna.cpu.commands.impl.LdyAbsolute;
import de.audioattack.erna.cpu.commands.impl.LdyAbsoluteX;
import de.audioattack.erna.cpu.commands.impl.LdyImmediate;
import de.audioattack.erna.cpu.commands.impl.LdyZeropage;
import de.audioattack.erna.cpu.commands.impl.LdyZeropageX;
import de.audioattack.erna.cpu.commands.impl.Nop;
import de.audioattack.erna.cpu.commands.impl.Pha;
import de.audioattack.erna.cpu.commands.impl.Php;
import de.audioattack.erna.cpu.commands.impl.Pla;
import de.audioattack.erna.cpu.commands.impl.Plp;
import de.audioattack.erna.cpu.commands.impl.Rti;
import de.audioattack.erna.cpu.commands.impl.Rts;
import de.audioattack.erna.cpu.commands.impl.Sec;
import de.audioattack.erna.cpu.commands.impl.Sed;
import de.audioattack.erna.cpu.commands.impl.Sei;
import de.audioattack.erna.cpu.commands.impl.StaAbsolute;
import de.audioattack.erna.cpu.commands.impl.StaAbsoluteX;
import de.audioattack.erna.cpu.commands.impl.StaAbsoluteY;
import de.audioattack.erna.cpu.commands.impl.StaIndirectX;
import de.audioattack.erna.cpu.commands.impl.StaIndirectY;
import de.audioattack.erna.cpu.commands.impl.StaZeropage;
import de.audioattack.erna.cpu.commands.impl.StaZeropageX;
import de.audioattack.erna.cpu.commands.impl.StxAbsolute;
import de.audioattack.erna.cpu.commands.impl.StxZeropage;
import de.audioattack.erna.cpu.commands.impl.StxZeropageY;
import de.audioattack.erna.cpu.commands.impl.StyAbsolute;
import de.audioattack.erna.cpu.commands.impl.StyZeropage;
import de.audioattack.erna.cpu.commands.impl.StyZeropageX;
import de.audioattack.erna.cpu.commands.impl.Tax;
import de.audioattack.erna.cpu.commands.impl.Tay;
import de.audioattack.erna.cpu.commands.impl.Tsx;
import de.audioattack.erna.cpu.commands.impl.Txa;
import de.audioattack.erna.cpu.commands.impl.Txs;
import de.audioattack.erna.cpu.commands.impl.Tya;
import de.audioattack.erna.util.ByteUtil;

import java.util.stream.Stream;

/**
 * Contains all commands of the CPU and allows to look them up by their opcode.
 */
// What can we do about that?
@SuppressWarnings({"ClassDataAbstractionCoupling", "ClassFanOutComplexity"})
public final class CommandMap {

    private static final Command[] COMMANDS = new Command[ByteUtil.ALL_BITS_SET];

    /**
     * Private constructor to avoid instantiation of static utility class.
     */
    private CommandMap() {
    }

    static {
        Stream.of(
                new BitAbsolute(),
                new BitZeropage(),
                new Bcc(),
                new Bcs(),
                new Beq(),
                new Bmi(),
                new Bne(),
                new Bpl(),
                new Brk(),
                new Bvc(),
                new Bvs(),
                new Clc(),
                new Cld(),
                new Cli(),
                new Clv(),
                new DecAbsolute(),
                new DecAbsoluteX(),
                new DecZeropage(),
                new DecZeropageX(),
                new Dex(),
                new Dey(),
                new IncAbsolute(),
                new IncAbsoluteX(),
                new IncZeropage(),
                new IncZeropageX(),
                new Inx(),
                new Iny(),
                new JmpAbsolute(),
                new JmpIndirect(),
                new Jsr(),
                new LdaAbsolute(),
                new LdaAbsoluteX(),
                new LdaAbsoluteY(),
                new LdaImmediate(),
                new LdaIndirectX(),
                new LdaIndirectY(),
                new LdaZeropage(),
                new LdaZeropageX(),
                new LdxAbsolute(),
                new LdxAbsoluteY(),
                new LdxImmediate(),
                new LdxZeropage(),
                new LdxZeropageY(),
                new LdyAbsolute(),
                new LdyAbsoluteX(),
                new LdyImmediate(),
                new LdyZeropage(),
                new LdyZeropageX(),
                new Nop(),
                new Pha(),
                new Php(),
                new Pla(),
                new Plp(),
                new Rti(),
                new Rts(),
                new Sec(),
                new Sed(),
                new Sei(),
                new StaAbsolute(),
                new StaAbsoluteX(),
                new StaAbsoluteY(),
                new StaIndirectX(),
                new StaIndirectY(),
                new StaZeropage(),
                new StaZeropageX(),
                new StyAbsolute(),
                new StyZeropage(),
                new StyZeropageX(),
                new StxAbsolute(),
                new StxZeropage(),
                new StxZeropageY(),
                new Tax(),
                new Tay(),
                new Tsx(),
                new Txa(),
                new Txs(),
                new Tya()

        ).forEach(c -> COMMANDS[ByteUtil.toUnsigned(c.getOpcode())] = c);

    }

    /**
     * Gets a command by its opcode.
     *
     * @param opcode an opcode
     * @return the corresponding command
     */
    public static Command get(final Byte opcode) {
        return COMMANDS[ByteUtil.toUnsigned(opcode)];
    }
}
