/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page LDX command. The LDX command copies a value from memory to the X-register.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#LDX">
 * http://www.6502.org/tutorials/6502opcodes.html#LDX</a>
 */
public class LdxZeropage extends LdxBase {

    private static final byte OPCODE = (byte) 0xA6;

    private static final byte COMMAND_LENGTH = 2;

    private static final byte CYCLES = 3;

    /**
     * Constructor.
     */
    public LdxZeropage() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ZEROPAGE, CYCLES);
    }

}
