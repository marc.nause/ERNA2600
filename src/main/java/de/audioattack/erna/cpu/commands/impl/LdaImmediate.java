/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Immediate LDA command. The LDA command copies a value from memory to the accumulator.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#LDX">
 * http://www.6502.org/tutorials/6502opcodes.html#LDX</a>
 */
public class LdaImmediate extends LdaBase {

    private static final byte OPCODE = (byte) 0xA9;

    private static final byte COMMAND_LENGTH = 2;

    private static final byte CYCLES = 2;

    /**
     * Constructor.
     */
    public LdaImmediate() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.IMMEDIATE, CYCLES);
    }

}
