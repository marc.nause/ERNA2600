/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Absolute JMP command. The JMP command transfers the program execution to the following address.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#JMP">
 * http://www.6502.org/tutorials/6502opcodes.html#JMP</a>
 */
public class JmpAbsolute extends JmpBase {

    private static final byte OPCODE = (byte) 0x4C;

    private static final byte COMMAND_LENGTH = (byte) 3;

    private static final byte CYCLES = 3;

    /**
     * Constructor.
     */
    public JmpAbsolute() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ABSOLUTE, CYCLES);
    }

}
