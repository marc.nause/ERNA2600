/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.addressing;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;

/**
 * Calculates an address according to a certain addressing type.
 */
public interface Addressing {

    /**
     * Calculates the address from the byte(s) following the address the program counter points to and increments the
     * program counter as required.
     *
     * @param cpu            the CPU
     * @param programCounter program counter of the CPU
     * @return the address
     */
    AddressResult getAddress(Cpu cpu, ProgramCounter programCounter);
}
