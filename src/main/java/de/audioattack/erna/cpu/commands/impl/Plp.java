/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.AbstractBaseCommand;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * PLP command. The PLP command pulls a value from the stack and puts the value into the status flags.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#PLP">
 * http://www.6502.org/tutorials/6502opcodes.html#PLP</a>
 */
public class Plp extends AbstractBaseCommand {

    private static final byte OPCODE = (byte) 0x28;

    private static final byte COMMAND_LENGTH = (byte) 1;

    private static final byte CYCLES = 4;

    /**
     * Constructor.
     */
    public Plp() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.IMPLIED);
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {
        stackPointer.increment();
        cpu.setStatus(cpu.getMemory(stackPointer));
        programCounter.increment();
        return CYCLES;
    }
}
