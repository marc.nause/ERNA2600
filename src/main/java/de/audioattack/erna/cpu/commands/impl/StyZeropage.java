/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page STY command. The STY command copies a value of the Y register to given zero-page address.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#STY">
 * http://www.6502.org/tutorials/6502opcodes.html#STY</a>
 */
public class StyZeropage extends StyBase {

    private static final byte OPCODE = (byte) 0x84;

    private static final byte COMMAND_LENGTH = (byte) 2;

    private static final byte CYCLES = 3;

    /**
     * Constructor.
     */
    public StyZeropage() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ZEROPAGE, CYCLES);
    }

}
