/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page STA command. The STA command copies a value of the accumulator to given absolute Y-indexed address.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#STA">
 * http://www.6502.org/tutorials/6502opcodes.html#STA</a>
 */
public class StaAbsoluteY extends StaBase {

    private static final byte OPCODE = (byte) 0x99;

    private static final byte COMMAND_LENGTH = (byte) 3;

    private static final byte CYCLES = 5;

    /**
     * Constructor.
     */
    public StaAbsoluteY() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.Y_INDEXED_ABSOLUTE, CYCLES);
    }

}
