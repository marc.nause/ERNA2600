/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.AbstractBaseCommand;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * RTS command. The RTS returns from a subroutine. It pulls the top two bytes off the stack and transfers
 * program control to that address+1
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#RTS">
 * http://www.6502.org/tutorials/6502opcodes.html#RTS</a>
 */
public class Rts extends AbstractBaseCommand {

    private static final byte OPCODE = (byte) 0x60;

    private static final byte COMMAND_LENGTH = (byte) 1;

    private static final byte CYCLES = 6;

    /**
     * Constructor.
     */
    public Rts() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.IMPLIED);
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {

        stackPointer.increment();
        final byte lo = cpu.getMemory(stackPointer);
        stackPointer.increment();
        final byte hi = cpu.getMemory(stackPointer);

        programCounter.set(lo, hi);
        // the stack contained PC - 1, we need to increment it to get correct address
        programCounter.increment();

        return CYCLES;
    }
}
