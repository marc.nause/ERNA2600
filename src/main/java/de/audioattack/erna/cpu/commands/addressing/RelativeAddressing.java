/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.addressing;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;

/**
 * Calculates a relative address.
 */
class RelativeAddressing implements Addressing {

    @Override
    public AddressResult getAddress(final Cpu cpu, final ProgramCounter programCounter) {
        programCounter.increment();
        final byte offset = cpu.getMemory(programCounter);

        /* Increment one more time since the relative address is relative to the address of the next command. */
        programCounter.increment();

        // ignore boundary crossed from AddressCalculator method call and check HI byte in this method
        final AddressResult result = AddressCalculator.addSigned(programCounter, offset);

        return new AddressResult(result.getAddress(), result.getAddress().getHi() != programCounter.getHi());
    }
}
