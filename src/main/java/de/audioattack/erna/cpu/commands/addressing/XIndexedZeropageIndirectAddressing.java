/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.addressing;

import de.audioattack.erna.cpu.AddressVectorImpl;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;

/**
 * Location n + X and next of page 0 hold address of data.
 */
class XIndexedZeropageIndirectAddressing implements Addressing {

    @Override
    public AddressResult getAddress(final Cpu cpu, final ProgramCounter programCounter) {
        programCounter.increment();
        final byte pointerLo = (byte) (cpu.getMemory(programCounter) + cpu.getX());

        final byte lo = cpu.getMemory(pointerLo, (byte) 0x00);
        final byte hi = cpu.getMemory((byte) (pointerLo + 1), (byte) 0x00);

        return new AddressResult(new AddressVectorImpl(lo, hi), false);
    }
}
