/*
 * SPDX-FileCopyrightText: 2023 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.AddressVector;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.AbstractBaseCommand;
import de.audioattack.erna.cpu.commands.addressing.AddressResult;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;
import de.audioattack.erna.util.ByteUtil;

/**
 * Base class for BIT command implementations. The BIT command does a bitwise
 * compare of a memory location and the accumulator.
 */
class BitBase extends AbstractBaseCommand {

    private static final int INDEX_BIT_6 = 6;

    private static final int INDEX_BIT_7 = 7;

    private final byte cycles;

    /**
     * Constructor.
     *
     * @param opcode         opcode of command
     * @param numberOfBytes  length of command in bytes
     * @param addressingType addressing type of command
     * @param cycles         duration of command in CPU cycles
     */
    protected BitBase(
            final byte opcode,
            final byte numberOfBytes,
            final AddressingType addressingType,
            final byte cycles) {
        super(opcode, numberOfBytes, addressingType);

        this.cycles = cycles;
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {

        final AddressResult result = getAddress(cpu, programCounter);
        final AddressVector address = result.getAddress();

        final byte valMemory = cpu.getMemory(address);
        final byte valAccumulator = cpu.getA();

        statusRegister.setZero((valMemory & valAccumulator) == 0);
        statusRegister.setNegative(ByteUtil.isBitSet(valMemory, INDEX_BIT_6));
        statusRegister.setOverflow(ByteUtil.isBitSet(valMemory, INDEX_BIT_7));

        programCounter.increment();

        return cycles;
    }
}
