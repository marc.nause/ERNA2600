/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Absolute LDA command. The LDA command copies a value from memory to the accumulator.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#LDA">
 * http://www.6502.org/tutorials/6502opcodes.html#LDA</a>
 */
public class LdaAbsolute extends LdaBase {

    private static final byte OPCODE = (byte) 0xAD;

    private static final byte COMMAND_LENGTH = 3;

    private static final byte CYCLES = 4;

    /**
     * Constructor.
     */
    public LdaAbsolute() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ABSOLUTE, CYCLES);
    }

}
