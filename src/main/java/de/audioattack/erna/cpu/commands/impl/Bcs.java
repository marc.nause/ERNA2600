/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.StatusRegister;

/**
 * BCS command. The BCS command branches if carry is set.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#BCS">
 * http://www.6502.org/tutorials/6502opcodes.html#BCS</a>
 */
public class Bcs extends BranchBase {

    private static final byte OPCODE = (byte) 0xB0;

    /**
     * Constructor.
     */
    public Bcs() {
        super(OPCODE, StatusRegister::isCarrySet);
    }
}
