/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

/**
 * BVC command. The BVC command branches if overflow flag is not set.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#BVC">
 * http://www.6502.org/tutorials/6502opcodes.html#BVC</a>
 */
public class Bvc extends BranchBase {

    private static final byte OPCODE = (byte) 0x50;

    /**
     * Constructor.
     */
    public Bvc() {
        super(OPCODE, statusRegister -> !statusRegister.isOverflowSet());
    }
}
