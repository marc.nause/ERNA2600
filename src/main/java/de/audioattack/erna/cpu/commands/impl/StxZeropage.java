/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page STX command. The STX command copies a value of the X register to given zero-page address.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#STX">
 * http://www.6502.org/tutorials/6502opcodes.html#STX</a>
 */
public class StxZeropage extends StxBase {

    private static final byte OPCODE = (byte) 0x86;

    private static final byte COMMAND_LENGTH = (byte) 2;

    private static final byte CYCLES = 3;

    /**
     * Constructor.
     */
    public StxZeropage() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ZEROPAGE, CYCLES);
    }

}
