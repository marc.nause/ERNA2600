/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Absolute LDY command. The LDY command copies a value from memory to the Y-register.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#LDY">
 * http://www.6502.org/tutorials/6502opcodes.html#LDY</a>
 */
public class LdyAbsolute extends LdyBase {

    private static final byte OPCODE = (byte) 0xAC;

    private static final byte COMMAND_LENGTH = 3;

    private static final byte CYCLES = 4;

    /**
     * Constructor.
     */
    public LdyAbsolute() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ABSOLUTE, CYCLES);
    }

}
