/*
 * SPDX-FileCopyrightText: 2023 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page X-indexed DEC command. The DEC decrements a zero-page memory location.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#DEC">
 * http://www.6502.org/tutorials/6502opcodes.html#DEC</a>
 */
public class DecZeropageX extends DecBase {

    private static final byte OPCODE = (byte) 0xC6;

    private static final byte COMMAND_LENGTH = 2;

    private static final byte CYCLES = 7;

    /**
     * Constructor.
     */
    public DecZeropageX() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.X_INDEXED_ZEROPAGE, CYCLES);
    }

}
