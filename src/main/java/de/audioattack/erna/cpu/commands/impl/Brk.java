/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.AddressVector;
import de.audioattack.erna.cpu.AddressVectorImpl;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.AbstractBaseCommand;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * BRK command. The BRK command causes a non-maskable interrupt and increments the program counter by one.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#BRK">
 * http://www.6502.org/tutorials/6502opcodes.html#BRK</a>
 */
public class Brk extends AbstractBaseCommand {

    private static final byte OPCODE = (byte) 0x00;

    private static final byte COMMAND_LENGTH = (byte) 1;

    private static final byte CYCLES = 7;

    /**
     * Constructor.
     */
    public Brk() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.IMPLIED);
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {

        statusRegister.setBrk(true);

        final ProgramCounter copyOfProgramCounter = new ProgramCounter();
        programCounter.set(programCounter);
        programCounter.increment();
        programCounter.increment();
        cpu.setMemory(stackPointer, copyOfProgramCounter.getLo());
        stackPointer.decrement();
        cpu.setMemory(stackPointer, copyOfProgramCounter.getHi());
        stackPointer.decrement();
        cpu.setMemory(stackPointer, statusRegister.toByte());
        stackPointer.decrement();

        final AddressVector irqVector = new AddressVectorImpl(cpu.getMemory(Cpu.IRQ_LO), cpu.getMemory(Cpu.IRQ_HI));
        programCounter.set(irqVector);

        statusRegister.setInterruptDisable(true);
        cpu.requestNmi();

        programCounter.increment();

        return CYCLES;
    }
}
