/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.StatusRegister;

/**
 * BMI command. The BMI command branches if negative flag is set.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#BMI">
 * http://www.6502.org/tutorials/6502opcodes.html#BMI</a>
 */
public class Bmi extends BranchBase {

    private static final byte OPCODE = (byte) 0x30;

    /**
     * Constructor.
     */
    public Bmi() {
        super(OPCODE, StatusRegister::isNegativeSet);
    }
}
