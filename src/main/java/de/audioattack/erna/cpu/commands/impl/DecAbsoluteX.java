/*
 * SPDX-FileCopyrightText: 2023 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Zero-page X-indexed DEC command. The DEC decrements the value in the following memory location.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#DEC">
 * http://www.6502.org/tutorials/6502opcodes.html#DEC</a>
 */
public class DecAbsoluteX extends DecBase {

    private static final byte OPCODE = (byte) 0xDE;

    private static final byte COMMAND_LENGTH = 3;

    private static final byte CYCLES = 7;

    /**
     * Constructor.
     */
    public DecAbsoluteX() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.X_INDEXED_ABSOLUTE, CYCLES);
    }

}
