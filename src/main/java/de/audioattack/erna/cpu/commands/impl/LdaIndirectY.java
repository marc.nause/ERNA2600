/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.addressing.AddressingType;

/**
 * Indirect X-indexed LDA command. The LDA command copies a value from memory to the accumulator.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#LDA">
 * http://www.6502.org/tutorials/6502opcodes.html#LDA</a>
 */
public class LdaIndirectY extends LdaBase {

    private static final byte OPCODE = (byte) 0xB1;

    private static final byte COMMAND_LENGTH = 2;

    private static final byte CYCLES = 5;

    /**
     * Constructor.
     */
    public LdaIndirectY() {
        super(OPCODE, COMMAND_LENGTH, AddressingType.ZEROPAGE_INDIRECT_Y_INDEXED, CYCLES);
    }

    @Override
    public byte run(
            final Cpu cpu,
            final StatusRegister statusRegister,
            final ProgramCounter programCounter,
            final StackPointer stackPointer) {
        byte cycles = super.run(cpu, statusRegister, programCounter, stackPointer);
        if (isPageBoundaryCrossed()) {
            cycles++;
        }
        return cycles;
    }
}
