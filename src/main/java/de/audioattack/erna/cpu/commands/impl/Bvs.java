/*
 * SPDX-FileCopyrightText: 2025 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.StatusRegister;

/**
 * BVS command. The BVS command branches if overflow flag is et.
 *
 * @see <a href="http://www.6502.org/tutorials/6502opcodes.html#BVS">
 * http://www.6502.org/tutorials/6502opcodes.html#BVS</a>
 */
public class Bvs extends BranchBase {

    private static final byte OPCODE = (byte) 0x70;

    /**
     * Constructor.
     */
    public Bvs() {
        super(OPCODE, StatusRegister::isOverflowSet);
    }
}
