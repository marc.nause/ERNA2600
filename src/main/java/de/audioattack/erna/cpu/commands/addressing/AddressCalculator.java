/*
 * SPDX-FileCopyrightText: 2024 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu.commands.addressing;

import de.audioattack.erna.cpu.AddressVector;
import de.audioattack.erna.cpu.AddressVectorImpl;

final class AddressCalculator {

    /**
     * Private constructor to avoid instantiation of static utility class.
     */
    private AddressCalculator() {
    }

    /**
     * Adds a value to an address.
     *
     * @param address the address
     * @param value   the value
     * @return result of the calculation
     */
    public static AddressResult add(final AddressVector address, final byte value) {
        return add(address.getLo(), address.getHi(), value);
    }

    /**
     * Adds a value to an address.
     *
     * @param lo    lo byte of the address
     * @param hi    hi byte of the address
     * @param value the value
     * @return result of the calculation
     */
    @SuppressWarnings({"BooleanExpressionComplexity", "MagicNumber"})
    public static AddressResult add(final byte lo, final byte hi, final byte value) {
        final byte newLo = (byte) (lo + value);
        final boolean isPageBoundaryCrossed = (newLo & 0xFF) < (value & 0xFF) || (newLo & 0xFF) < (lo & 0xFF);

        return new AddressResult(
                new AddressVectorImpl(newLo, isPageBoundaryCrossed ? (byte) (hi + 1) : hi),
                isPageBoundaryCrossed);
    }

    /**
     * Adds a value to an address, treats the value as a signed number.
     *
     * @param address the address
     * @param value   the value
     * @return result of the calculation
     */
    public static AddressResult addSigned(final AddressVector address, final byte value) {
        return addSigned(address.getLo(), address.getHi(), value);
    }

    /**
     * Adds a value to an address, treats the value as a signed number.
     *
     * @param lo    lo byte of the address
     * @param hi    hi byte of the address
     * @param value the value
     * @return result of the calculation
     */
    @SuppressWarnings({"BooleanExpressionComplexity", "MagicNumber"})
    public static AddressResult addSigned(final byte lo, final byte hi, final byte value) {
        final byte newLo = (byte) (lo + value);
        final boolean isPageBoundaryCrossedPos = value > 0 && (newLo & 0xFF) < (lo & 0xFF);
        final boolean isPageBoundaryCrossedNeg = value < 0 && (newLo & 0xFF) > (lo & 0xFF);

        final byte offsetHi;
        if (isPageBoundaryCrossedPos) {
            offsetHi = 1;
        } else if (isPageBoundaryCrossedNeg) {
            offsetHi = -1;
        } else {
            offsetHi = 0;
        }

        return new AddressResult(
                new AddressVectorImpl(newLo, (byte) (hi + offsetHi)),
                isPageBoundaryCrossedPos || isPageBoundaryCrossedNeg);
    }
}
