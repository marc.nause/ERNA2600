/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu;

import de.audioattack.erna.util.ByteUtil;

/**
 * Program counter, a vector which can be incremented and decremented.
 */
public class ProgramCounter implements AddressVector, Incrementeable, Decrementeable {

    private byte pcHi;

    private byte pcLo;

    /**
     * Sets program counter to new value.
     *
     * @param address the new value
     */
    public void set(final AddressVector address) {
        this.pcLo = address.getLo();
        this.pcHi = address.getHi();
    }

    /**
     * Sets program counter to new value.
     *
     * @param newLo lo byte of new value
     * @param newHi hi byte of new value
     */
    public void set(final byte newLo, final byte newHi) {
        this.pcLo = newLo;
        this.pcHi = newHi;
    }

    @Override
    public byte getHi() {
        return pcHi;
    }

    @Override
    public byte getLo() {
        return pcLo;
    }

    @Override
    public void increment() {
        pcLo++;
        if (ByteUtil.toUnsigned(pcLo) == 0) {
            pcHi++;
        }
    }

    @Override
    public void decrement() {
        pcLo--;
        if (ByteUtil.toUnsigned(pcLo) == ByteUtil.ALL_BITS_SET) {
            pcHi--;
        }
    }

    @Override
    public String toString() {
        return "ProgramCounter{"
                + "pcLo=" + pcLo
                + ", pcHi=" + pcHi
                + '}';
    }
}
