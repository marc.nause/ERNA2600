/*
 * SPDX-FileCopyrightText: 2020 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu;

import de.audioattack.erna.cpu.commands.CommandMap;
import de.audioattack.erna.memory.Ram;

import java.util.Objects;

/**
 * Implementation of the MOS 6502 CPU.
 */
public class Mos6502 implements Cpu {


    private enum State {
        EXECUTING_COMMAND,
        READY
    }

    private enum InterruptState {
        IRQ_REQUESTED,
        RESET_REQUESTED,
        NMI_REQUESTED,
        HANDLING_INTERRUPT,
        NONE
    }

    /**
     * Number of clock cycles required for interrupt handling <a href="http://www.6502.org/tutorials/interrupts.html">
     * according to the Interwebs™</a>.
     */
    private static final byte INTERRUPT_CYCLES = 7;

    private final StatusRegister statusRegister = new StatusRegister();

    private final Ram memory;

    private byte accumulator;

    private byte indexX;

    private byte indexY;

    private final ProgramCounter pc = new ProgramCounter();

    private final StackPointer stackPointer = new StackPointer();

    private State executionState;

    private InterruptState interruptState;

    private byte commandCycleCounter;

    /**
     * Constructor.
     *
     * @param memory memory the CPU can access
     */
    public Mos6502(final Ram memory) {
        this.memory = Objects.requireNonNull(memory, "memory must not be <null>");
        reset();
        tick();
    }

    @Override
    public void reset() {
        interruptState = InterruptState.RESET_REQUESTED;
        executionState = State.READY;
    }

    /**
     * Gets current NMI vector of the system.
     *
     * @return NMI vector
     */
    public AddressVector getNmiVector() {
        return new AddressVectorImpl(memory.get(NMI_LO), memory.get(NMI_HI));
    }

    /**
     * Gets current IRQ vector of the system.
     *
     * @return IRQ vector
     */
    public AddressVector getIrqVector() {
        return new AddressVectorImpl(memory.get(IRQ_LO), memory.get(IRQ_HI));
    }

    /**
     * Gets current Reset vector of the system.
     *
     * @return Reset vector
     */
    public AddressVector getResetVector() {
        return new AddressVectorImpl(memory.get(RESET_LO), memory.get(RESET_HI));
    }

    @Override
    public void requestIrq() {
        if (!statusRegister.isInterruptDisableSet()) {
            interruptState = InterruptState.IRQ_REQUESTED;
        }
    }

    @Override
    public void requestNmi() {
        interruptState = InterruptState.NMI_REQUESTED;
    }

    @Override
    public AddressVector getCurrentPc() {
        return new AddressVectorImpl(pc);
    }

    @Override
    public AddressVector getStackPointer() {
        return new AddressVectorImpl(stackPointer);
    }

    @Override
    public StatusRegister getCurrentStatus() {
        return new StatusRegister(statusRegister);
    }

    @Override
    public void setStatus(final byte status) {
        statusRegister.set(status);
    }

    @Override
    public byte getX() {
        return indexX;
    }

    @Override
    public void setA(final byte newValue) {
        accumulator = newValue;
    }

    @Override
    public byte getA() {
        return accumulator;
    }

    @Override
    public void setX(final byte newValue) {
        indexX = newValue;
    }

    @Override
    public byte getY() {
        return indexY;
    }

    @Override
    public void setY(final byte newValue) {
        indexY = newValue;
    }

    @Override
    public byte getMemory(final byte lo, final byte hi) {
        return memory.get(lo, hi);
    }

    @Override
    public byte getMemory(final AddressVector memAddress) {
        return getMemory(memAddress.getLo(), memAddress.getHi());
    }

    @Override
    public void setMemory(final byte lo, final byte hi, final byte value) {
        memory.set(lo, hi, value);
    }

    @Override
    public void setMemory(final AddressVector memAddress, final byte newValue) {
        setMemory(memAddress.getLo(), memAddress.getHi(), newValue);
    }

    @Override
    public void tick() {

        if (executionState == State.READY) {
            if (interruptState == InterruptState.NONE) {
                executionState = State.EXECUTING_COMMAND;
                commandCycleCounter =
                        CommandMap.get(memory.get(pc.getLo(), pc.getHi())).
                                run(this, statusRegister, pc, stackPointer);
            } else if (interruptState == InterruptState.IRQ_REQUESTED) {
                interruptState = InterruptState.HANDLING_INTERRUPT;
                pushStateToStack();
                pc.set(getIrqVector());
                commandCycleCounter = INTERRUPT_CYCLES;
            } else if (interruptState == InterruptState.NMI_REQUESTED) {
                executionState = State.EXECUTING_COMMAND;
                interruptState = InterruptState.HANDLING_INTERRUPT;
                pushStateToStack();
                pc.set(getNmiVector());
                commandCycleCounter = INTERRUPT_CYCLES;
            } else if (interruptState == InterruptState.RESET_REQUESTED) {
                interruptState = InterruptState.HANDLING_INTERRUPT;
                pc.set(getResetVector());
                /*
                 * Setting the cycle counter to 1 is probably not correct, but I don't want to
                 * add several ticks to every unit test I have written so far just to wait until
                 * the CPU is ready before starting the actual test. This may mess up the timing
                 * if the reset vector is used in a program to jump somewhere, but as long as we
                 * don't know if this leads to problems, let's just keep it like this.
                 */
                commandCycleCounter = 1;
            }
        }


        if (--commandCycleCounter == 0) {
            executionState = State.READY;
            if (interruptState == InterruptState.HANDLING_INTERRUPT) {
                interruptState = InterruptState.NONE;
            }
        }
    }

    private void pushStateToStack() {
        memory.set(stackPointer.getLo(), stackPointer.getHi(), pc.getHi());
        stackPointer.decrement();
        memory.set(stackPointer.getLo(), stackPointer.getHi(), pc.getLo());
        stackPointer.decrement();
        memory.set(stackPointer.getLo(), stackPointer.getHi(), statusRegister.toByte());
        stackPointer.decrement();
    }

    @Override
    public String toString() {
        return "Mos6502{"
                + "statusFlags=" + statusRegister
                + ", accumulator=" + accumulator
                + ", indexX=" + indexX
                + ", indexY=" + indexY
                + ", pc=" + pc
                + ", stackPointer=" + stackPointer
                + '}';
    }
}
