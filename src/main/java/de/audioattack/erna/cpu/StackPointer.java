/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu;

import de.audioattack.erna.util.ByteUtil;

/**
 * Pointer for current position on the stack.
 */
public class StackPointer implements AddressVector, Incrementeable, Decrementeable {


    private static final byte INITIAL_POS_STACKPOINTER = (byte) 0xFD;

    private byte lo = INITIAL_POS_STACKPOINTER;

    /**
     * Set address in stack.
     *
     * @param spLo the address
     */
    public void setLo(final byte spLo) {
        lo = spLo;
    }

    @Override
    public byte getHi() {
        return 0x01;
    }

    @Override
    public byte getLo() {
        return lo;
    }

    @Override
    public void increment() {
        lo++;
    }

    @Override
    public void decrement() {
        lo--;
    }

    @Override
    public String toString() {
        return "$" + ByteUtil.toHexString(getHi()) + ByteUtil.toHexString(lo);
    }

}
