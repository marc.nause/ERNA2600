/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu;

import de.audioattack.erna.util.ByteUtil;

import java.util.Objects;

/**
 * Implementation of pointer to a 16-bit memory address.
 */
public class AddressVectorImpl implements AddressVector {

    private final byte hi;

    private final byte lo;

    /**
     * Constructor.
     *
     * @param lo low byte of the address vector
     * @param hi high byte of the address vector
     */
    public AddressVectorImpl(final byte lo, final byte hi) {
        this.lo = lo;
        this.hi = hi;
    }

    /**
     * Copy Constructor.
     *
     * @param address address vector to copy
     */
    public AddressVectorImpl(final AddressVector address) {
        this.lo = address.getLo();
        this.hi = address.getHi();
    }

    @Override
    public byte getHi() {
        return hi;
    }

    @Override
    public byte getLo() {
        return lo;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AddressVectorImpl that = (AddressVectorImpl) o;
        return hi == that.hi && lo == that.lo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hi, lo);
    }

    @Override
    public String toString() {
        return "$" + ByteUtil.toHexString(hi) + ByteUtil.toHexString(lo);
    }
}
