/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu;

/**
 * Something that can be decremented.
 */
public interface Decrementeable {

    /**
     * Decrements one step.
     */
    void decrement();
}
