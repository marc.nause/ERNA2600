/*
 * SPDX-FileCopyrightText: 2020 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu;

/**
 * Status flags of the CPU.
 */
public class StatusRegister {

    /**
     * Expansion flag. Is always set tp 'true'.
     */
    private static final boolean EXPANSION = true;

    /**
     * Negative flag. Matches the high bit of the result of operation the CPU has just completed.
     */
    private boolean negative;

    /**
     * Overflow flag.
     */
    private boolean overflow;

    /**
     * Break flag, Set when BRK instruction is executed, clear at all other times.
     */
    private boolean brk;

    /**
     * Decimal flag. When set, the CPU does its arithmetic in BCD.
     */
    private boolean decimal;

    /**
     * Interrupt mask. When this set, the computer will not honor interrupts.
     */
    private boolean interruptDisable;

    /**
     * Zero flag. Set if the result of any operation is zero.
     */
    private boolean zero;

    /**
     * Carry flag. Set whenever the accumulator rolls over from $FF to $00. Also set by
     * various rotation and comparison instructions.
     */
    private boolean carry;

    /**
     * Constructor.
     */
    public StatusRegister() {
    }

    /**
     * Copy constructor.
     *
     * @param statusRegister status register to copy
     */
    public StatusRegister(final StatusRegister statusRegister) {
        this.negative = statusRegister.negative;
        this.overflow = statusRegister.overflow;
        this.brk = statusRegister.brk;
        this.decimal = statusRegister.decimal;
        this.interruptDisable = statusRegister.interruptDisable;
        this.zero = statusRegister.zero;
        this.carry = statusRegister.carry;
    }

    /**
     * Tells if negative flag is set.
     *
     * @return {@code true} if flag is set, else {@code false}
     */
    public boolean isNegativeSet() {
        return negative;
    }

    /**
     * Sets negative flag.
     *
     * @param negative {@code true} sto set flag, {@code false} to clear flag
     */
    public void setNegative(final boolean negative) {
        this.negative = negative;
    }

    /**
     * Tells if overflow flag is set.
     *
     * @return {@code true} if flag is set, else {@code false}
     */
    public boolean isOverflowSet() {
        return overflow;
    }

    /**
     * Sets overflow flag.
     *
     * @param overflow {@code true} if flag is set, else {@code false}
     */
    public void setOverflow(final boolean overflow) {
        this.overflow = overflow;
    }

    /**
     * Tells if brk flag is set.
     *
     * @return {@code true} if flag is set, else {@code false}
     */
    public boolean isBrkSet() {
        return brk;
    }

    /**
     * Sets btk flag.
     *
     * @param brk {@code true} if flag is set, else {@code false}
     */
    public void setBrk(final boolean brk) {
        this.brk = brk;
    }

    /**
     * Tells if decimal flag is set.
     *
     * @return {@code true} if flag is set, else {@code false}
     */
    public boolean isDecimalSet() {
        return decimal;
    }

    /**
     * Sets decimal flag.
     *
     * @param decimal {@code true} if flag is set, else {@code false}
     */
    public void setDecimal(final boolean decimal) {
        this.decimal = decimal;
    }

    /**
     * Tells if interrupt disabled flag is set.
     *
     * @return {@code true} if flag is set, else {@code false}
     */
    public boolean isInterruptDisableSet() {
        return interruptDisable;
    }

    /**
     * Sets interrupt disabled flag.
     *
     * @param interruptDisable {@code true} if flag is set, else {@code false}
     */
    public void setInterruptDisable(final boolean interruptDisable) {
        this.interruptDisable = interruptDisable;
    }

    /**
     * Tells if zero flag is set.
     *
     * @return {@code true} if flag is set, else {@code false}
     */
    public boolean isZeroSet() {
        return zero;
    }

    /**
     * Sets zero flag.
     *
     * @param zero {@code true} if flag is set, else {@code false}
     */
    public void setZero(final boolean zero) {
        this.zero = zero;
    }

    /**
     * Tells if carry flag is set.
     *
     * @return {@code true} if flag is set, else {@code false}
     */
    public boolean isCarrySet() {
        return carry;
    }

    /**
     * Sets carry flag.
     *
     * @param carry {@code true} if flag is set, else {@code false}
     */
    public void setCarry(final boolean carry) {
        this.carry = carry;
    }

    /**
     * Tells if expansion flag is set.
     *
     * @return always {@code true}
     */
    public boolean isExpansionSet() {
        return EXPANSION;
    }

    private byte toByte(final boolean value) {
        return (byte) (value ? 0x01 : 0x00);
    }

    /**
     * Gets status register as byte.
     *
     * @return the status register as byte
     */
    @SuppressWarnings({"BooleanExpressionComplexity", "MagicNumber"})
    public byte toByte() {
        return (byte) (toByte(negative) << 7
                | toByte(overflow) << 6
                | toByte(EXPANSION) << 5
                | toByte(brk) << 4
                | toByte(decimal) << 3
                | toByte(interruptDisable) << 2
                | toByte(zero) << 1
                | toByte(carry));
    }

    /**
     * Sets all flags of the status register from a byte.
     *
     * @param statusByte the new values
     */
    @SuppressWarnings({"BooleanExpressionComplexity", "MagicNumber"})
    public void set(final byte statusByte) {
        negative = (statusByte & 0xFF & 0x01 << 7) == 0x01 << 7;
        overflow = (statusByte & 0xFF & 0x01 << 6) == 0x01 << 6;
        brk = (statusByte & 0xFF & 0x01 << 4) == 0x01 << 4;
        decimal = (statusByte & 0xFF & 0x01 << 3) == 0x01 << 3;
        interruptDisable = (statusByte & 0xFF & 0x01 << 2) == 0x01 << 2;
        zero = (statusByte & 0xFF & 0x01 << 1) == 0x01 << 1;
        carry = (statusByte & 0xFF & 0x01) == 0x01;
    }

    @Override
    public String toString() {
        return "StatusFlags{"
                + "negative=" + negative
                + ", overflow=" + overflow
                + ", expansion=" + EXPANSION
                + ", brk=" + brk
                + ", decimal=" + decimal
                + ", interruptDisable=" + interruptDisable
                + ", zero=" + zero
                + ", carry=" + carry + '}';
    }
}
