/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu;

/**
 * A pointer to a 16-bit memory address.
 */
public interface AddressVector {

    /**
     * Gets high byte of address vector.
     *
     * @return the high byte
     */
    byte getHi();

    /**
     * Gets low byte of address vector.
     *
     * @return the low byte
     */
    byte getLo();
}
