/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.cpu;

/**
 * Something that can be incremented.
 */
public interface Incrementeable {

    /**
     * Increments one step.
     */
    void increment();
}
