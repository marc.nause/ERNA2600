/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * Utility classes which are potentially useful for all packages in this project.
 */
package de.audioattack.erna.util;
