/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.util;

/**
 * Utility class with convenience methods for byte handling.
 */
public final class ByteUtil {

    /**
     * Byte will all bits set. hex: FF, dec: 255
     */
    public static final int ALL_BITS_SET = 0xFF;

    /**
     * Byte with the highest bit set.
     */
    private static final int HIGHEST_BIT_SET = 0B10000000;

    private static final String BYTE_FORMAT = "%02x";

    /**
     * Private constructor to avoid initialisation of static utility class.
     */
    private ByteUtil() {
    }

    /**
     * Converts two byte values to an int.
     *
     * @param lo low byte
     * @param hi high byte
     * @return integer calculated from the two input bytes
     */
    @SuppressWarnings("checkstyle:magicnumber")
    public static int toInt(final byte lo, final byte hi) {
        return ((hi & 0xFF) << 8) | (lo & 0xFF);
    }

    /**
     * Mask a byte with 0xFF and return it as an int.
     *
     * @param b byte to mask
     * @return value of unsigned byte
     */
    @SuppressWarnings("checkstyle:magicnumber")
    public static int toUnsigned(final byte b) {
        return b & 0xFF;
    }

    /**
     * Tells if byte contains negative number (highest bit is set).
     *
     * @param b the number
     * @return {@code true} if negative, else {@code false}
     */
    public static boolean isNegative(final byte b) {
        return (b & HIGHEST_BIT_SET) == HIGHEST_BIT_SET;
    }

    /**
     * Gets hexadecimal representation of a byte.
     *
     * @param b the byte
     * @return hexadecimal representation
     */
    public static String toHexString(final byte b) {
        return String.format(BYTE_FORMAT, b);
    }

    /**
     * Checks if a bit of a byte is set or not.
     *
     * @param b     the byte
     * @param index index of bit in byte, 0 is least significant bit
     * @return {@code true} if bit is set, else {@code false}
     */
    public static boolean isBitSet(final byte b, final int index) {
        return ((b >> index) & 1) == 1;
    }
}
