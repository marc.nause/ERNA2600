/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.memory;

import de.audioattack.erna.cpu.AddressVector;
import de.audioattack.erna.util.ByteUtil;

import java.time.temporal.ValueRange;
import java.util.AbstractMap;
import java.util.function.IntFunction;

/**
 * Combines several memory ranges to one memory map.
 */
public class MemoryMapping implements Ram {

    private final RangeMap<Rom> mapping = new RangeMap<>();

    /**
     * Adds a memory range to the memory mapping.
     *
     * @param memory the memory to add
     * @param offset offset of the memory to add
     */
    public void add(final Rom memory, final int offset) {
        mapping.put(ValueRange.of(offset, offset + memory.size() - 1), memory);
    }

    @Override
    public void set(final byte lo, final byte hi, final byte value) {
        set(ByteUtil.toInt(lo, hi), value);
    }

    @Override
    public void set(final AddressVector memAddress, final byte value) {
        set(memAddress.getLo(), memAddress.getHi(), value);
    }

    @Override
    public void set(final int memAddress, final byte value) {
        final AbstractMap.SimpleEntry<Long, Rom> entry = mapping.get(memAddress);
        if (entry != null) {
            final Rom rom = entry.getValue();
            if (rom instanceof Ram) {
                ((Ram) rom).set(memAddress - entry.getKey().intValue(), value);
            }
        }
    }

    @Override
    public void fill(final byte value) {
        fill(i -> value);
    }

    @Override
    public void fill(final IntFunction<Byte> function) {
        mapping.entrySet().forEach(valueRangeRomEntry -> {
            final Rom rom = valueRangeRomEntry.getValue();
            if (rom instanceof Ram) {
                ((Ram) rom).fill(function);
            }
        });
    }

    @Override
    public byte get(final byte lo, final byte hi) {
        return get(ByteUtil.toInt(lo, hi));
    }

    @Override
    public byte get(final AddressVector memAddress) {
        return get(memAddress.getLo(), memAddress.getHi());
    }

    @Override
    public byte get(final int memAddress) {
        final AbstractMap.SimpleEntry<Long, Rom> entry = mapping.get(memAddress);
        final Rom rom = entry.getValue();
        return rom.get(memAddress - entry.getKey().intValue());
    }

    @Override
    public int size() {

        final Holder holder = new Holder();

        mapping.entrySet().forEach(valueRangeRomEntry -> {
            final ValueRange valueRange = valueRangeRomEntry.getKey();
            holder.minimum = Math.min(holder.minimum, valueRange.getMinimum());
            holder.maximum = Math.max(holder.maximum, valueRange.getMaximum() + 1);
        });

        return (int) (holder.maximum - holder.minimum);
    }

    private static final class Holder {

        private long minimum;

        private long maximum;
    }
}
