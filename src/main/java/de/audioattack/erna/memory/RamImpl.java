/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.memory;

import de.audioattack.erna.cpu.AddressVector;
import de.audioattack.erna.util.ByteUtil;

import java.util.Arrays;
import java.util.function.IntFunction;

/**
 * Implementation of memory which can be read and written.
 */
public class RamImpl implements Ram {

    private final byte[] memory;

    /**
     * Constructor.
     *
     * @param size size of memory
     */
    public RamImpl(final int size) {
        memory = new byte[size];
    }

    @Override
    public byte get(final int memAddress) {
        return memory[memAddress];
    }

    @Override
    public byte get(final byte lo, final byte hi) {
        return get(ByteUtil.toInt(lo, hi));
    }

    @Override
    public byte get(final AddressVector memAddress) {
        return get(memAddress.getLo(), memAddress.getHi());
    }

    @Override
    public void set(final int memAddress, final byte value) {
        memory[memAddress] = value;
    }

    @Override
    public void set(final byte lo, final byte hi, final byte value) {
        set(ByteUtil.toInt(lo, hi), value);
    }

    @Override
    public void set(final AddressVector memAddress, final byte value) {
        set(memAddress.getLo(), memAddress.getHi(), value);
    }

    @Override
    public int size() {
        return memory.length;
    }

    @Override
    public void fill(final byte value) {
        Arrays.fill(memory, value);
    }

    @Override
    public void fill(final IntFunction<Byte> function) {
        for (int i = 0; i < memory.length; i++) {
            memory[i] = function.apply(i);
        }
    }

}
