/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.memory;

import java.time.temporal.ValueRange;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Maps objects to a range of values.
 *
 * @param <T> type of the mapped objects
 */
class RangeMap<T> {

    private final Map<ValueRange, T> map = new HashMap<>();

    /**
     * Adds new key/value pair. Does not check if key intersects with another key which is contained in the map already.
     * It is the developer's responsibility to make sure that no intersection occur (if this matches the use case).
     *
     * @param key   given range
     * @param value value associated with range
     */
    public void put(final ValueRange key, final T value) {

        map.put(key, value);
    }

    /**
     * Gets entry of map.
     *
     * @param rangeMember value within range
     * @return the entry which is mapped to the value
     */
    public AbstractMap.SimpleEntry<Long, T> get(final long rangeMember) {

        final Optional<Map.Entry<ValueRange, T>> valueRange
                = map.entrySet().
                stream().
                filter(e -> e.getKey().isValidValue(rangeMember)).
                findAny();

        return valueRange.map(e -> new AbstractMap.SimpleEntry<>(e.getKey().getMinimum(), e.getValue())).orElse(null);
    }

    /**
     * Gets all entries of the map.
     *
     * @return all entries
     */
    public Set<Map.Entry<ValueRange, T>> entrySet() {
        return map.entrySet();
    }
}
