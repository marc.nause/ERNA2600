/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.memory;

import de.audioattack.erna.cpu.AddressVector;

/**
 * Memory which can only be read.
 */
public interface Rom {

    /**
     * Gets current value of a memory cell.
     *
     * @param lo low byte of address of memory cell
     * @param hi high byte of address of memory cell
     * @return current value
     */
    byte get(byte lo, byte hi);

    /**
     * Gets current value of a memory cell.
     *
     * @param memAddress address of memory cell
     * @return current value
     */
    byte get(int memAddress);

    /**
     * Gets current value of a memory cell.
     *
     * @param memAddress address of memory cell
     * @return current value
     */
    byte get(AddressVector memAddress);

    /**
     * Gets size of ROM.
     *
     * @return the size
     */
    int size();
}
