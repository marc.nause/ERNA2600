/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.memory;

import de.audioattack.erna.cpu.AddressVector;
import de.audioattack.erna.util.ByteUtil;

import java.util.Arrays;

/**
 * Implementation of memory which can only be read.
 */
public class RomImpl implements Rom {

    private final byte[] memory;

    /**
     * Constructor.
     *
     * @param size size of memory
     * @param data data to fill memory with
     */
    public RomImpl(final int size, final byte[] data) {
        memory = Arrays.copyOf(data, size);
    }

    /**
     * Constructor, size of memory will be size of data.
     *
     * @param data data to fill memory with
     */
    public RomImpl(final byte[] data) {
        this(data.length, data);
    }

    @Override
    public byte get(final byte lo, final byte hi) {
        return get(ByteUtil.toInt(lo, hi));
    }

    @Override
    public byte get(final int memAddress) {
        return memory[memAddress];
    }

    @Override
    public byte get(final AddressVector memAddress) {
        return get(memAddress.getLo(), memAddress.getHi());
    }

    @Override
    public int size() {
        return memory.length;
    }

}
