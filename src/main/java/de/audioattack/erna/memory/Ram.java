/*
 * SPDX-FileCopyrightText: 2020 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

package de.audioattack.erna.memory;

import de.audioattack.erna.cpu.AddressVector;

import java.util.function.IntFunction;

/**
 * Memory which can be read and written.
 */
public interface Ram extends Rom {

    /**
     * Sets value of memory cell.
     *
     * @param lo    low byte of address of memory cell
     * @param hi    high byte of address of memory cell
     * @param value the new value
     */
    void set(byte lo, byte hi, byte value);

    /**
     * Sets value of memory cell.
     *
     * @param memAddress address of memory cell
     * @param value      the new value
     */
    void set(int memAddress, byte value);

    /**
     * Sets value of memory cell.
     *
     * @param memAddress address of memory cell
     * @param value      the new value
     */
    void set(AddressVector memAddress, byte value);

    /**
     * Fills memory with a value.
     *
     * @param value the value
     */
    void fill(byte value);

    /**
     * Fills memory with value calculated by a function.
     *
     * @param function the function
     */
    void fill(IntFunction<Byte> function);

}
