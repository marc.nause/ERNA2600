/*
 * SPDX-FileCopyrightText: 2022 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * Classes which are related to memory handling.
 */
package de.audioattack.erna.memory;
