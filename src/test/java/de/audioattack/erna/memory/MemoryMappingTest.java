package de.audioattack.erna.memory;

import de.audioattack.erna.cpu.AddressVectorImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MemoryMappingTest {

    @Test
    void testAddAndGet() {

        final Ram ram1 = new RamImpl(256);
        ram1.fill((byte) 0x01);
        final Ram ram2 = new RamImpl(256);
        ram2.fill((byte) 0x02);

        final MemoryMapping memoryMapping = new MemoryMapping();
        memoryMapping.add(ram1, 0);
        memoryMapping.add(ram2, ram1.size());

        assertEquals((byte) 0x01, memoryMapping.get(new AddressVectorImpl((byte) 0x00, (byte) 0x00)));
        assertEquals((byte) 0x01, memoryMapping.get(new AddressVectorImpl((byte) 0xFF, (byte) 0x00)));
        assertEquals((byte) 0x02, memoryMapping.get(new AddressVectorImpl((byte) 0x00, (byte) 0x01)));
        assertEquals((byte) 0x02, memoryMapping.get(new AddressVectorImpl((byte) 0xFF, (byte) 0x01)));
    }

    @Test
    void testFill() {

        final Ram ram1 = new RamImpl(256);
        ram1.fill((byte) 0x01);
        final Ram ram2 = new RamImpl(256);
        ram2.fill((byte) 0x02);

        final MemoryMapping memoryMapping = new MemoryMapping();
        memoryMapping.add(ram1, 0);
        memoryMapping.add(ram2, ram1.size());

        memoryMapping.fill((byte) 0x03);

        assertEquals((byte) 0x03, memoryMapping.get(new AddressVectorImpl((byte) 0x00, (byte) 0x00)));
        assertEquals((byte) 0x03, memoryMapping.get(new AddressVectorImpl((byte) 0xFF, (byte) 0x00)));
        assertEquals((byte) 0x03, memoryMapping.get(new AddressVectorImpl((byte) 0x00, (byte) 0x01)));
        assertEquals((byte) 0x03, memoryMapping.get(new AddressVectorImpl((byte) 0xFF, (byte) 0x01)));
    }

    @Test
    void testSetAndGet() {

        final Ram ram1 = new RamImpl(256);
        final Ram ram2 = new RamImpl(256);

        final MemoryMapping memoryMapping = new MemoryMapping();
        memoryMapping.add(ram1, 0);
        memoryMapping.add(ram2, ram1.size());

        memoryMapping.fill((byte) 0xFF);

        memoryMapping.set(new AddressVectorImpl((byte) 0x00, (byte) 0x00), (byte) 0x04);
        memoryMapping.set(new AddressVectorImpl((byte) 0xFF, (byte) 0x00), (byte) 0x05);
        memoryMapping.set(new AddressVectorImpl((byte) 0x00, (byte) 0x01), (byte) 0x06);
        memoryMapping.set(new AddressVectorImpl((byte) 0xFF, (byte) 0x01), (byte) 0x07);

        assertEquals((byte) 0x04, memoryMapping.get(new AddressVectorImpl((byte) 0x00, (byte) 0x00)));
        assertEquals((byte) 0x05, memoryMapping.get(new AddressVectorImpl((byte) 0xFF, (byte) 0x00)));
        assertEquals((byte) 0x06, memoryMapping.get(new AddressVectorImpl((byte) 0x00, (byte) 0x01)));
        assertEquals((byte) 0x07, memoryMapping.get(new AddressVectorImpl((byte) 0xFF, (byte) 0x01)));
        assertEquals((byte) 0xFF, memoryMapping.get(new AddressVectorImpl((byte) 0xEE, (byte) 0x01)));
    }

    @Test
    void testSizEmpty() {
        final MemoryMapping memoryMapping = new MemoryMapping();

        assertEquals(0, memoryMapping.size());
    }

    @Test
    void testSizeSimple() {
        final Ram ram1 = new RamImpl(256);

        final MemoryMapping memoryMapping = new MemoryMapping();
        memoryMapping.add(ram1, 0);

        assertEquals(ram1.size(), memoryMapping.size());
    }

    @Test
    void testSize() {
        final Ram ram1 = new RamImpl(256);
        final Ram ram2 = new RamImpl(256);

        final MemoryMapping memoryMapping = new MemoryMapping();
        memoryMapping.add(ram1, 0);
        memoryMapping.add(ram2, ram1.size());

        assertEquals(ram1.size() + ram2.size(), memoryMapping.size());
    }

    @Test
    void testSizeWithHole() {
        final Ram ram1 = new RamImpl(256);
        final Ram ram2 = new RamImpl(256);

        final MemoryMapping memoryMapping = new MemoryMapping();
        memoryMapping.add(ram1, 0);
        memoryMapping.add(ram2, 2 * ram1.size());

        assertEquals(ram1.size() * 2 + ram2.size(), memoryMapping.size());
    }
}
