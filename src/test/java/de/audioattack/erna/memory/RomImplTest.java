package de.audioattack.erna.memory;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RomImplTest {

    @Test
    void testConstructor() {
        final Rom memory = new RomImpl(2, new byte[]{(byte) 0, (byte) -123, (byte) 127});
        assertEquals(0, memory.get((byte) 0, (byte) 0));
        assertEquals(-123, memory.get((byte) 1, (byte) 0));

        assertEquals(2, memory.size());
    }

    @Test
    void testConstructor2() {
        final Rom memory = new RomImpl(4, new byte[]{(byte) 0, (byte) -123, (byte) 127});
        assertEquals(0, memory.get((byte) 0, (byte) 0));
        assertEquals(-123, memory.get((byte) 1, (byte) 0));
        assertEquals(127, memory.get((byte) 2, (byte) 0));
        assertEquals(0, memory.get((byte) 3, (byte) 0));

        assertEquals(4, memory.size());
    }

    @Test
    void testGet() {
        final Rom memory = new RomImpl(new byte[]{(byte) 0, (byte) -123, (byte) 127});
        assertEquals(0, memory.get((byte) 0, (byte) 0));
        assertEquals(-123, memory.get((byte) 1, (byte) 0));
        assertEquals(127, memory.get((byte) 2, (byte) 0));
    }

    @Test
    void testSize() {
        final Rom memory = new RomImpl(new byte[]{(byte) 0, (byte) -123, (byte) 127});
        assertEquals(3, memory.size());
    }

}