package de.audioattack.erna.memory;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RamImplTest {

    @Test
    void get() {
        final Ram memory = new RamImpl(16);
        assertEquals(0, memory.get((byte) 0, (byte) 0));
        memory.fill((byte) 0xff);
        for (byte i = 0; i < memory.size(); i++) {
            assertEquals((byte) 0xff, memory.get(i, (byte) 0));
        }
    }

    @Test
    void set() {
        final Ram memory = new RamImpl(512);
        assertEquals(0, memory.get((byte) 2, (byte) 1));
        memory.set((byte) 2, (byte) 1, (byte) 0xff);
        assertEquals((byte) 0xff, memory.get((byte) 2, (byte) 1));
    }

    @Test
    void testGet() {
        final Ram memory = new RamImpl(16);
        assertEquals(0, memory.get(0));
        memory.fill((byte) 0xff);
        for (byte i = 0; i < memory.size(); i++) {
            assertEquals((byte) 0xff, memory.get(i));
        }
    }

    @Test
    void testSet() {
        final Ram memory = new RamImpl(512);
        assertEquals(0, memory.get(499));
        memory.set(499, (byte) 0xff);
        assertEquals((byte) 0xff, memory.get(499));
    }

    @Test
    void fill() {
        final Ram memory = new RamImpl(16);
        memory.fill((byte) 2);
        for (int i = 0; i < 16; i++) {
            assertEquals((byte) 2, memory.get(i));
        }
        memory.fill((byte) 3);
        for (int i = 0; i < 16; i++) {
            assertEquals((byte) 3, memory.get(i));
        }
    }

    @Test
    void testFill() {
        final Ram memory = new RamImpl(16);
        memory.fill(i -> (byte) i);
        for (int i = 0; i < 16; i++) {
            assertEquals((byte) i, memory.get(i));
        }
        memory.fill(i -> (byte) (16 - i));
        for (int i = 0; i < 16; i++) {
            assertEquals((byte) (16 - i), memory.get(i));
        }
    }
}