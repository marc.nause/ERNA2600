package de.audioattack.erna.memory;

import org.junit.jupiter.api.Test;

import java.time.temporal.ValueRange;
import java.util.AbstractMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class RangeMapTest {

    @Test
    void get() {
        final RangeMap<Long> rangeMap = new RangeMap<>();
        assertNull(rangeMap.get(0));
        rangeMap.put(ValueRange.of(0, 1), 123L);
        assertEquals(new AbstractMap.SimpleEntry<>(0L, 123L), rangeMap.get(0));
        assertEquals(new AbstractMap.SimpleEntry<>(0L, 123L), rangeMap.get(1));
        assertNull(rangeMap.get(2));
        rangeMap.put(ValueRange.of(2, 1000), 124L);
        assertEquals(new AbstractMap.SimpleEntry<>(2L, 124L), rangeMap.get(2));
        assertEquals(new AbstractMap.SimpleEntry<>(2L, 124L), rangeMap.get(50));
        assertEquals(new AbstractMap.SimpleEntry<>(2L, 124L), rangeMap.get(1000));
    }
}