package de.audioattack.erna.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ByteUtilTest {

    @Test
    void toInt() {
        byte hi = 0;
        byte lo = 0;
        for (int i = 0; i < Math.pow(2, 16); i++) {

            assertEquals(i, ByteUtil.toInt(lo, hi));
            lo++;
            if (lo == 0) {
                hi++;
            }
        }
    }

    @Test
    void isBitSet() {
        byte b = (byte) 0b01010101;

        for (int i = 0; i < 8; i++) {
            boolean isBitSet = ByteUtil.isBitSet(b, i);
            if (i % 2 == 0) {
                assertTrue(isBitSet);
            } else {
                assertFalse(isBitSet);
            }
        }
    }
}