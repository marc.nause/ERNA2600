package de.audioattack.erna.cpu;

import de.audioattack.erna.cpu.commands.impl.Nop;
import de.audioattack.erna.cpu.commands.impl.Rti;
import de.audioattack.erna.cpu.commands.impl.Sei;
import de.audioattack.erna.memory.Ram;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class InterruptIT {

    @Test
    void runRegularIrqTest() {

        final Ram memory = new RamImpl(256 * 256);
        // set values for PC, will be looked up by CPU during initialization
        memory.set(Mos6502.RESET_LO, (byte) 0x01);
        memory.set(Mos6502.RESET_HI, (byte) 0x00);
        // set values for IRQ, will be used when interrupt is requested
        memory.set(Mos6502.IRQ_LO, (byte) 0x02);
        memory.set(Mos6502.IRQ_HI, (byte) 0x01);

        memory.set((byte) 0x01, (byte) 0x00, new Nop().getOpcode());           // $0001 NOP
        memory.set((byte) 0x02, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x03, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x04, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x05, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x06, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x07, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x08, (byte) 0x00, new Nop().getOpcode());

        memory.set((byte) 0x02, (byte) 0x01, new Rti().getOpcode());

        final Cpu cpu = new Mos6502(memory);
        // set carry flag
        cpu.setStatus((byte) 0x01);

        // check PC after reset
        assertEquals((byte) 0x01, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        assertTrue(cpu.getCurrentStatus().isCarrySet());

        cpu.tick();
        cpu.requestIrq();

        assertEquals((byte) 0x02, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        // First NOP is done now.
        cpu.tick();

        // let interrupt do its thing (7 cycles)
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();

        // Jump to IRQ vector.
        assertEquals((byte) 0x02, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x01, cpu.getCurrentPc().getHi());

        // remove previously set values for status flags
        cpu.setStatus((byte) 0x00);

        assertFalse(cpu.getCurrentStatus().isCarrySet());

        // RTI takes 6 cycles.
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();

        // check if status flags have been restored
        assertTrue(cpu.getCurrentStatus().isCarrySet());

        // check if programm counter has been restored
        assertEquals((byte) 0x02, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());
    }

    @Test
    void runIrqWithIrqMaskSetTest() {

        final Ram memory = new RamImpl(256 * 256);
        // set values for PC, will be looked up by CPU during initialization
        memory.set(Mos6502.RESET_LO, (byte) 0x01);
        memory.set(Mos6502.RESET_HI, (byte) 0x00);
        // set values for IRQ, will be used when interrupt is requested
        memory.set(Mos6502.IRQ_LO, (byte) 0x02);
        memory.set(Mos6502.IRQ_HI, (byte) 0x01);

        memory.set((byte) 0x01, (byte) 0x00, new Sei().getOpcode());           // $0001 NOP
        memory.set((byte) 0x02, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x03, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x04, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x05, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x06, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x07, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x08, (byte) 0x00, new Nop().getOpcode());

        memory.set((byte) 0x02, (byte) 0x01, new Rti().getOpcode());

        final Cpu cpu = new Mos6502(memory);

        // check PC after reset
        assertEquals((byte) 0x01, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        assertFalse(cpu.getCurrentStatus().isInterruptDisableSet());

        cpu.tick();
        cpu.tick();

        assertTrue(cpu.getCurrentStatus().isInterruptDisableSet());

        // SEI is done now and IRQ request should be ignored.
        cpu.requestIrq();

        assertEquals((byte) 0x02, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        cpu.tick();
        cpu.tick();

        // NOP is done now.
        assertEquals((byte) 0x03, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());
    }

    @Test
    void runRegularNmiTest() {

        final Ram memory = new RamImpl(256 * 256);
        // set values for PC, will be looked up by CPU during initialization
        memory.set(Mos6502.RESET_LO, (byte) 0x01);
        memory.set(Mos6502.RESET_HI, (byte) 0x00);
        // set values for NMI, will be used when interrupt is requested
        memory.set(Mos6502.NMI_LO, (byte) 0x02);
        memory.set(Mos6502.NMI_HI, (byte) 0x01);

        memory.set((byte) 0x01, (byte) 0x00, new Nop().getOpcode());           // $0001 NOP
        memory.set((byte) 0x02, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x03, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x04, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x05, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x06, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x07, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x08, (byte) 0x00, new Nop().getOpcode());

        memory.set((byte) 0x02, (byte) 0x01, new Rti().getOpcode());

        final Cpu cpu = new Mos6502(memory);
        // set carry flag
        cpu.setStatus((byte) 0x01);

        // check PC after reset
        assertEquals((byte) 0x01, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        assertTrue(cpu.getCurrentStatus().isCarrySet());

        cpu.tick();
        cpu.requestNmi();

        assertEquals((byte) 0x02, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        // First NOP is done now.
        cpu.tick();

        // let interrupt do its thing (7 cycles)
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();

        // Jump to IRQ vector.
        assertEquals((byte) 0x02, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x01, cpu.getCurrentPc().getHi());

        // remove previously set values for status flags
        cpu.setStatus((byte) 0x00);

        assertFalse(cpu.getCurrentStatus().isCarrySet());

        // RTI takes 6 cycles.
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();

        // check if status flags have been restored
        assertTrue(cpu.getCurrentStatus().isCarrySet());

        // check if programm counter has been restored
        assertEquals((byte) 0x02, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());
    }

    @Test
    void runNmiWithIqMaskSetTest() {

        final Ram memory = new RamImpl(256 * 256);
        // set values for PC, will be looked up by CPU during initialization
        memory.set(Mos6502.RESET_LO, (byte) 0x01);
        memory.set(Mos6502.RESET_HI, (byte) 0x00);
        // set values for NMI, will be used when interrupt is requested
        memory.set(Mos6502.NMI_LO, (byte) 0x02);
        memory.set(Mos6502.NMI_HI, (byte) 0x01);

        memory.set((byte) 0x01, (byte) 0x00, new Sei().getOpcode());           // $0001 NOP
        memory.set((byte) 0x02, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x03, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x04, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x05, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x06, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x07, (byte) 0x00, new Nop().getOpcode());
        memory.set((byte) 0x08, (byte) 0x00, new Nop().getOpcode());

        memory.set((byte) 0x02, (byte) 0x01, new Rti().getOpcode());

        final Cpu cpu = new Mos6502(memory);
        // set carry flag
        cpu.setStatus((byte) 0x01);

        // check PC after reset
        assertEquals((byte) 0x01, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        assertFalse(cpu.getCurrentStatus().isInterruptDisableSet());

        cpu.tick();
        cpu.tick();

        assertTrue(cpu.getCurrentStatus().isInterruptDisableSet());

        // SEI is done now and IRQ request should be ignored, but we request NMI which should work.
        cpu.requestNmi();
        assertTrue(cpu.getCurrentStatus().isCarrySet());

        // let interrupt do its thing (7 cycles)
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();

        // Jump to IRQ vector.
        assertEquals((byte) 0x02, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x01, cpu.getCurrentPc().getHi());

        // remove previously set values for status flags
        cpu.setStatus((byte) 0x00);

        assertFalse(cpu.getCurrentStatus().isCarrySet());

        // RTI takes 6 cycles.
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();

        // check if status flags have been restored
        assertTrue(cpu.getCurrentStatus().isCarrySet());

        // check if programm counter has been restored
        assertEquals((byte) 0x02, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());
    }

}
