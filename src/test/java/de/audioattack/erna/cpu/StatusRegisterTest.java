package de.audioattack.erna.cpu;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StatusRegisterTest {

    @Test
    void toByte() {
        final StatusRegister status = new StatusRegister();
        assertEquals(0x01 << 5, status.toByte());
        status.setNegative(true);
        assertEquals((byte) (0x01 << 7 | 0x01 << 5), status.toByte());
        status.setOverflow(true);
        assertEquals((byte) (0x01 << 7 | 0x01 << 6 | 0x01 << 5), status.toByte());
        status.setBrk(true);
        assertEquals((byte) (0x01 << 7 | 0x01 << 6 | 0x01 << 5 | 0x01 << 4), status.toByte());
        status.setDecimal(true);
        assertEquals((byte) (0x01 << 7 | 0x01 << 6 | 0x01 << 5 | 0x01 << 4 | 0x01 << 3), status.toByte());
        status.setInterruptDisable(true);
        assertEquals((byte) (0x01 << 7 | 0x01 << 6 | 0x01 << 5 | 0x01 << 4 | 0x01 << 3 | 0x01 << 2), status.toByte());
        status.setZero(true);
        assertEquals((byte) (0x01 << 7 | 0x01 << 6 | 0x01 << 5 | 0x01 << 4 | 0x01 << 3 | 0x01 << 2 | 0x01 << 1), status.toByte());
        status.setCarry(true);
        assertEquals((byte) (0x01 << 7 | 0x01 << 6 | 0x01 << 5 | 0x01 << 4 | 0x01 << 3 | 0x01 << 2 | 0x01 << 1 | 0x01), status.toByte());
        status.setNegative(false);
        assertEquals((byte) (0x01 << 6 | 0x01 << 5 | 0x01 << 4 | 0x01 << 3 | 0x01 << 2 | 0x01 << 1 | 0x01), status.toByte());
        status.setOverflow(false);
        assertEquals((byte) (0x01 << 5 | 0x01 << 4 | 0x01 << 3 | 0x01 << 2 | 0x01 << 1 | 0x01), status.toByte());
        status.setBrk(false);
        assertEquals((byte) (0x01 << 5 | 0x01 << 3 | 0x01 << 2 | 0x01 << 1 | 0x01), status.toByte());
        status.setDecimal(false);
        assertEquals((byte) (0x01 << 5 | 0x01 << 2 | 0x01 << 1 | 0x01), status.toByte());
        status.setInterruptDisable(false);
        assertEquals((byte) (0x01 << 5 | 0x01 << 1 | 0x01), status.toByte());
        status.setZero(false);
        assertEquals((byte) (0x01 << 5 | 0x01), status.toByte());
        status.setCarry(false);
        assertEquals((byte) (0x01 << 5), status.toByte());
    }

    @Test
    void set() {
        final StatusRegister status = new StatusRegister();
        assertFalse(status.isNegativeSet());
        status.set((byte) (0x01 << 7 | 0x01 << 5));
        assertTrue(status.isNegativeSet());
        // TODO: add tests for all flags
    }
}