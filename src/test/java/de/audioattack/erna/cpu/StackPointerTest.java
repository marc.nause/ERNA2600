package de.audioattack.erna.cpu;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StackPointerTest {

    @Test
    void getHi() {
        assertEquals((byte) 0x01, new StackPointer().getHi());
    }

    @Test
    void getLo() {
        assertEquals((byte) 0xFD, new StackPointer().getLo());
    }

    @Test
    void increment() {
        final StackPointer stackPointer = new StackPointer();
        for (int i = 1; i < 512; i++) {
            stackPointer.increment();
            assertEquals((byte) 0x01, new StackPointer().getHi());
            assertEquals((byte) (0xFD + (i % 256)), stackPointer.getLo());
        }
    }

    @Test
    void decrement() {
        final StackPointer stackPointer = new StackPointer();
        for (int i = 1; i < 512; i++) {
            stackPointer.decrement();
            assertEquals((byte) 0x01, new StackPointer().getHi());
            assertEquals((byte) (0xFD - (i % 256)), stackPointer.getLo());
        }
    }
}