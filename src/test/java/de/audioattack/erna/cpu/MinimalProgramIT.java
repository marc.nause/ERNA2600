package de.audioattack.erna.cpu;

import de.audioattack.erna.cpu.commands.impl.JmpAbsolute;
import de.audioattack.erna.cpu.commands.impl.Nop;
import de.audioattack.erna.memory.Ram;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MinimalProgramIT {

    @Test
    void runMinimalProgram() {

        final Ram memory = new RamImpl(256 * 256);
        // set values for PC, will be looked up by CPU during initialization
        memory.set((byte) 0xFC, (byte) 0xFF, (byte) 0x01);
        memory.set((byte) 0xFD, (byte) 0xFF, (byte) 0x00);

        memory.set((byte) 0x00, (byte) 0x00, (byte) 0x02);          // $0000 $02 // no legal opcode
        memory.set((byte) 0x01, (byte) 0x00, new Nop().getOpcode());           // $0001 NOP
        memory.set((byte) 0x02, (byte) 0x00, new JmpAbsolute().getOpcode());   // $0002 JMP $01 $00
        memory.set((byte) 0x03, (byte) 0x00, (byte) 0x01);
        memory.set((byte) 0x04, (byte) 0x00, (byte) 0x00);
        memory.set((byte) 0x05, (byte) 0x00, (byte) 0x02);          // $0005 $02 // no legal opcode

        final Cpu cpu = new Mos6502(memory);

        // check PC after reset
        assertEquals((byte) 0x01, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        // run loop 3 times
        for (int i = 0; i < 3; i++) {

            // execute NOP
            cpu.tick();
            cpu.tick();

            assertEquals((byte) 0x02, cpu.getCurrentPc().getLo());
            assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

            // execute JMP
            cpu.tick();
            cpu.tick();
            cpu.tick();

            assertEquals((byte) 0x01, cpu.getCurrentPc().getLo());
            assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());
        }
    }

}
