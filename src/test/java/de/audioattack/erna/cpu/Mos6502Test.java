package de.audioattack.erna.cpu;

import de.audioattack.erna.memory.Ram;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Mos6502Test {

    @Test
    void init() {

        final Ram memory = new RamImpl(256 * 256);
        memory.set(Mos6502.RESET_LO, (byte) 0x01);
        memory.set(Mos6502.RESET_HI, (byte) 0x00);

        final Cpu cpu = new Mos6502(memory);
        assertEquals((byte) 0x01, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());
    }

}
