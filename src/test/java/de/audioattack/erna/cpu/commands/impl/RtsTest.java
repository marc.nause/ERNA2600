package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.Mos6502;
import de.audioattack.erna.memory.Ram;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RtsTest {

    @Test
    void run() {

        final Ram memory = new RamImpl(256 * 256);
        memory.fill((byte) 0xFF);
        // set values for PC, will be looked up by CPU during initialization
        memory.set((byte) 0xFC, (byte) 0xFF, (byte) 0x01);
        memory.set((byte) 0xFD, (byte) 0xFF, (byte) 0x00);

        memory.set((byte) 0x01, (byte) 0x00, new Txs().getOpcode()); // 0001 TXS
        memory.set((byte) 0x02, (byte) 0x00, new Rts().getOpcode()); // 0001 RTS
        memory.set((byte) 0xFC, (byte) 0x01, (byte) 0x07);         // write lo byte of return PC (-1) to stack
        memory.set((byte) 0xFD, (byte) 0x01, (byte) 0x00);         // write lo byte of return PC to stack

        final Cpu cpu = new Mos6502(memory);
        cpu.setX((byte) 0xFB); // set X for TXS at $0001

        // run TXS
        cpu.tick();
        cpu.tick();

        // check PC after reset
        assertEquals((byte) 0x02, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        assertEquals((byte) 0xFB, cpu.getStackPointer().getLo());
        assertEquals((byte) 0x01, cpu.getStackPointer().getHi());

        // RTS takes 6 cycles.
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();

        assertEquals((byte) 0x08, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        assertEquals((byte) 0xFD, cpu.getStackPointer().getLo());
        assertEquals((byte) 0x01, cpu.getStackPointer().getHi());
    }

}
