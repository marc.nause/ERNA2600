package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.Mos6502;
import de.audioattack.erna.cpu.commands.Command;
import de.audioattack.erna.memory.Ram;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JmpAbsoluteTest {

    @Test
    void run() {

        final Ram memory = new RamImpl(256 * 256);
        // set values for PC, will be looked up by CPU during initialization
        memory.set((byte) 0xFC, (byte) 0xFF, (byte) 0x01);
        memory.set((byte) 0xFD, (byte) 0xFF, (byte) 0x00);

        final Command command = new JmpAbsolute();
        memory.set((byte) 0x01, (byte) 0x00, command.getOpcode()); // 0001 JMP $34 $12
        memory.set((byte) 0x02, (byte) 0x00, (byte) 34);
        memory.set((byte) 0x03, (byte) 0x00, (byte) 12);

        final Cpu cpu = new Mos6502(memory);

        // check PC after reset
        assertEquals((byte) 0x01, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        // Absolute JMP takes 3 cycles.
        cpu.tick();
        cpu.tick();
        cpu.tick();

        assertEquals((byte) 34, cpu.getCurrentPc().getLo());
        assertEquals((byte) 12, cpu.getCurrentPc().getHi());
    }

}
