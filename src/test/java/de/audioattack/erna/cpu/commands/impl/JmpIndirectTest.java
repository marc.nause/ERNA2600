package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.Mos6502;
import de.audioattack.erna.cpu.commands.Command;
import de.audioattack.erna.memory.Ram;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JmpIndirectTest {

    @Test
    void run() {

        final Ram memory = new RamImpl(256 * 256);
        // set values for PC, will be looked up by CPU during initialization
        memory.set((byte) 0xFC, (byte) 0xFF, (byte) 0x01);
        memory.set((byte) 0xFD, (byte) 0xFF, (byte) 0x00);

        final Command command = new JmpIndirect();
        memory.set((byte) 0x01, (byte) 0x00, command.getOpcode()); // 0001 JMP ($34 $12)
        memory.set((byte) 0x02, (byte) 0x00, (byte) 0x34);
        memory.set((byte) 0x03, (byte) 0x00, (byte) 0x12);

        memory.set((byte) 0x34, (byte) 0x12, (byte) 0x22);
        memory.set((byte) 0x35, (byte) 0x12, (byte) 0x33);

        final Cpu cpu = new Mos6502(memory);

        // check PC after reset
        assertEquals((byte) 0x01, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        // Indirect JMP takes 5 cycles.
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();

        assertEquals((byte) 0x22, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x33, cpu.getCurrentPc().getHi());
    }

    @Test
    void runEdgeCase() {

        /*
         * An indirect jump does not increment the page address. This is a pitfall, but that's just the way it is.
         *
         * JMP ($FF $12) will fetch:
         *   - the low byte from $12FF
         *   - the high byte from $1200
         * */

        final Ram memory = new RamImpl(256 * 256);
        // set values for PC, will be looked up by CPU during initialization
        memory.set((byte) 0xFC, (byte) 0xFF, (byte) 0x01);
        memory.set((byte) 0xFD, (byte) 0xFF, (byte) 0x00);

        final Command command = new JmpIndirect();
        memory.set((byte) 0x01, (byte) 0x00, command.getOpcode()); // 0001 JMP ($FF $12)
        memory.set((byte) 0x02, (byte) 0x00, (byte) 0xFF);
        memory.set((byte) 0x03, (byte) 0x00, (byte) 0x12);

        memory.set((byte) 0xFF, (byte) 0x12, (byte) 0x22);
        memory.set((byte) 0x00, (byte) 0x12, (byte) 0x33);
        memory.set((byte) 0x00, (byte) 0x13, (byte) 0x44);

        final Cpu cpu = new Mos6502(memory);

        // check PC after reset
        assertEquals((byte) 0x01, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        // Indirect JMP takes 5 cycles.
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();

        assertEquals((byte) 0x22, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x33, cpu.getCurrentPc().getHi());
    }

}
