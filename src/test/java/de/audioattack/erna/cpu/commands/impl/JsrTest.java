package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.Mos6502;
import de.audioattack.erna.cpu.commands.Command;
import de.audioattack.erna.memory.Ram;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JsrTest {

    @Test
    void run() {

        final Ram memory = new RamImpl(256 * 256);
        memory.fill((byte) 0xFF);
        // set values for PC, will be looked up by CPU during initialization
        memory.set((byte) 0xFC, (byte) 0xFF, (byte) 0x01);
        memory.set((byte) 0xFD, (byte) 0xFF, (byte) 0x00);

        final Command command = new Jsr();
        memory.set((byte) 0x01, (byte) 0x00, command.getOpcode()); // 0001 JSR $34 $12
        memory.set((byte) 0x02, (byte) 0x00, (byte) 0x34);
        memory.set((byte) 0x03, (byte) 0x00, (byte) 0x12);

        final Cpu cpu = new Mos6502(memory);

        // check PC after reset
        assertEquals((byte) 0x01, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        assertEquals((byte) 0xFD, cpu.getStackPointer().getLo());
        assertEquals((byte) 0x01, cpu.getStackPointer().getHi());

        // JSR takes 6 cycles.
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();
        cpu.tick();

        assertEquals((byte) 0x34, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x12, cpu.getCurrentPc().getHi());

        assertEquals((byte) 0xFB, cpu.getStackPointer().getLo());
        assertEquals((byte) 0x01, cpu.getStackPointer().getHi());

        assertEquals((byte) 0x03, cpu.getMemory((byte) 0xFC, (byte) 0x01));
        assertEquals((byte) 0x00, cpu.getMemory((byte) 0xFD, (byte) 0x01));
    }

}
