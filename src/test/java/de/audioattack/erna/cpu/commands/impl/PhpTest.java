package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.AddressVectorImpl;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.Mos6502;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.Command;
import de.audioattack.erna.memory.Ram;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PhpTest {

    @Test
    void run() {
        final Cpu cpu = new Mos6502(new RamImpl(256 * 256));
        final StackPointer stackPointer = new StackPointer();
        final Command command = new Php();
        assertEquals((byte) 0x01 << 5, cpu.getCurrentStatus().toByte());
        assertEquals((byte) 0x00, cpu.getMemory(stackPointer));
        final int cycles = command.run(cpu, new StatusRegister(), new ProgramCounter(), stackPointer);
        assertEquals((byte) 0x01 << 5, cpu.getMemory((byte) (stackPointer.getLo() + 1), stackPointer.getHi()));
        assertEquals(3, cycles);
    }

    @Test
    void testFlagsAndPc() {
        final Ram memory = new RamImpl(256 * 256);
        // set values for PC, will be looked up by CPU during initialization
        memory.set((byte) 0xFC, (byte) 0xFF, (byte) 0x01);
        memory.set((byte) 0xFD, (byte) 0xFF, (byte) 0x00);

        memory.set((byte) 0x01, (byte) 0x00, new Php().getOpcode()); // 0001 PHP
        memory.set((byte) 0x02, (byte) 0x00, new JmpAbsolute().getOpcode()); // 0002 JMP 01 00
        memory.set((byte) 0x03, (byte) 0x00, (byte) 0x01);
        memory.set((byte) 0x04, (byte) 0x00, (byte) 0x00);

        final Cpu cpu = new Mos6502(memory);

        // TODO push something on stack

        for (int i = 0; i <= 260; i++) {

            assertEquals(new AddressVectorImpl((byte) 0x01, (byte) 0x00), cpu.getCurrentPc());

            // 3 cycles for PHP
            cpu.tick();
            cpu.tick();
            cpu.tick();

            assertEquals(new AddressVectorImpl((byte) 0x02, (byte) 0x00), cpu.getCurrentPc());

            // 3 cycles for absolute JMP
            cpu.tick();
            cpu.tick();
            cpu.tick();

            assertEquals(new AddressVectorImpl((byte) 0x01, (byte) 0x00), cpu.getCurrentPc());
        }
    }
}