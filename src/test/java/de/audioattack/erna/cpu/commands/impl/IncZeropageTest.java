package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.Mos6502;
import de.audioattack.erna.cpu.commands.Command;
import de.audioattack.erna.memory.Ram;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class IncZeropageTest {

    @Test
    void incToZeroTest() {

        final Ram memory = new RamImpl(256 * 256);
        // set values for PC, will be looked up by CPU during initialization
        memory.set((byte) 0xFC, (byte) 0xFF, (byte) 0x01);
        memory.set((byte) 0xFD, (byte) 0xFF, (byte) 0x00);

        final Command command = new IncZeropage();
        memory.set((byte) 0x01, (byte) 0x00, command.getOpcode()); // 0001 INC $04
        memory.set((byte) 0x02, (byte) 0x00, (byte) 0x04);
        memory.set((byte) 0x03, (byte) 0x00, (byte) 0x00);
        memory.set((byte) 0x04, (byte) 0x00, (byte) 0xFF);

        final Cpu cpu = new Mos6502(memory);

        // check PC after reset
        assertEquals((byte) 0x01, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        // Zero-Page DEC takes 2 cycles.
        cpu.tick();
        cpu.tick();

        assertEquals((byte) 0x00, cpu.getMemory((byte) 0x04, (byte) 0x00));
        assertTrue(cpu.getCurrentStatus().isZeroSet());
        assertFalse(cpu.getCurrentStatus().isNegativeSet());

        assertEquals((byte) 0x03, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());
    }

    @Test
    void incToNegativeTest() {

        final Ram memory = new RamImpl(256 * 256);
        // set values for PC, will be looked up by CPU during initialization
        memory.set((byte) 0xFC, (byte) 0xFF, (byte) 0x01);
        memory.set((byte) 0xFD, (byte) 0xFF, (byte) 0x00);

        final Command command = new IncZeropage();
        memory.set((byte) 0x01, (byte) 0x00, command.getOpcode()); // 0001 INC $04
        memory.set((byte) 0x02, (byte) 0x00, (byte) 0x04);
        memory.set((byte) 0x03, (byte) 0x00, (byte) 0x00);
        memory.set((byte) 0x04, (byte) 0x00, (byte) 0x81);

        final Cpu cpu = new Mos6502(memory);

        // check PC after reset
        assertEquals((byte) 0x01, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        // Zero-Page DEC takes 2 cycles.
        cpu.tick();
        cpu.tick();

        assertEquals((byte) 0x82, cpu.getMemory((byte) 0x04, (byte) 0x00));
        assertFalse(cpu.getCurrentStatus().isZeroSet());
        assertTrue(cpu.getCurrentStatus().isNegativeSet());

        assertEquals((byte) 0x03, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());
    }

    @Test
    void incToPositiveTest() {

        final Ram memory = new RamImpl(256 * 256);
        // set values for PC, will be looked up by CPU during initialization
        memory.set((byte) 0xFC, (byte) 0xFF, (byte) 0x01);
        memory.set((byte) 0xFD, (byte) 0xFF, (byte) 0x00);

        final Command command = new IncZeropage();
        memory.set((byte) 0x01, (byte) 0x00, command.getOpcode()); // 0001 INC $04
        memory.set((byte) 0x02, (byte) 0x00, (byte) 0x04);
        memory.set((byte) 0x03, (byte) 0x00, (byte) 0x00);
        memory.set((byte) 0x04, (byte) 0x00, (byte) 0x00);

        final Cpu cpu = new Mos6502(memory);

        // check PC after reset
        assertEquals((byte) 0x01, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());

        // Zero-Page DEC takes 2 cycles.
        cpu.tick();
        cpu.tick();

        assertEquals((byte) 0x01, cpu.getMemory((byte) 0x04, (byte) 0x00));
        assertFalse(cpu.getCurrentStatus().isZeroSet());
        assertFalse(cpu.getCurrentStatus().isNegativeSet());

        assertEquals((byte) 0x03, cpu.getCurrentPc().getLo());
        assertEquals((byte) 0x00, cpu.getCurrentPc().getHi());
    }

}
