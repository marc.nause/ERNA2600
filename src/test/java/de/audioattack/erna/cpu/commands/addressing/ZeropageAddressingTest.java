package de.audioattack.erna.cpu.commands.addressing;

import de.audioattack.erna.cpu.AddressVectorImpl;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.Mos6502;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class ZeropageAddressingTest {

    @Test
    void getAddressSimple() {

        final Addressing calculator = new XIndexedZeropageAddressing();
        final Cpu cpu = new Mos6502(new RamImpl(256 * 256));

        cpu.setMemory((byte) 0x01, (byte) 0x00, (byte) 0x05);

        final ProgramCounter programCounter = new ProgramCounter();
        programCounter.set((byte) 0x00, (byte) 0x00);

        final AddressResult address = calculator.getAddress(cpu, programCounter);
        assertEquals(new AddressVectorImpl((byte) 0x05, (byte) 0x00), address.getAddress());
        assertFalse(address.isPageBoundaryCrossed());
    }
}
