package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.AddressVectorImpl;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.Mos6502;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.Command;
import de.audioattack.erna.memory.Ram;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ClvTest {

    @Test
    void run() {
        final Cpu cpu = new Mos6502(new RamImpl(256 * 256));
        final StatusRegister statusRegister = new StatusRegister();
        statusRegister.setOverflow(true);
        final Command command = new Clv();
        assertTrue(statusRegister.isOverflowSet());
        final int cycles = command.run(cpu, statusRegister, new ProgramCounter(), new StackPointer());
        assertFalse(statusRegister.isOverflowSet());
        assertEquals(2, cycles);
    }

    @Test
    void testFlagsAndPc() {
        final Ram memory = new RamImpl(256 * 256);
        // set values for PC, will be looked up by CPU during initialization
        memory.set((byte) 0xFC, (byte) 0xFF, (byte) 0x01);
        memory.set((byte) 0xFD, (byte) 0xFF, (byte) 0x00);

        memory.set((byte) 0x01, (byte) 0x00, new Clv().getOpcode()); // 0001 CLV
        memory.set((byte) 0x02, (byte) 0x00, new JmpAbsolute().getOpcode()); // 0002 JMP 01 00
        memory.set((byte) 0x03, (byte) 0x00, (byte) 0x01);
        memory.set((byte) 0x04, (byte) 0x00, (byte) 0x00);

        final Cpu cpu = new Mos6502(memory);

        // TODO set flag to true to be able to test later

        for (int i = 0; i <= 10; i++) {

            assertEquals(new AddressVectorImpl((byte) 0x01, (byte) 0x00), cpu.getCurrentPc());

            // 2 cycles for CLV
            cpu.tick();
            cpu.tick();

            assertEquals(new AddressVectorImpl((byte) 0x02, (byte) 0x00), cpu.getCurrentPc());

            // 3 cycles for absolute JMP
            cpu.tick();
            cpu.tick();
            cpu.tick();

            assertEquals(new AddressVectorImpl((byte) 0x01, (byte) 0x00), cpu.getCurrentPc());

            assertFalse(cpu.getCurrentStatus().isCarrySet());
        }
    }
}