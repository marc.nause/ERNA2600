package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.AddressVectorImpl;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.Mos6502;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.Command;
import de.audioattack.erna.memory.Ram;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DexTest {

    @Test
    void run() {
        final Cpu cpu = new Mos6502(new RamImpl(256 * 256));
        final Command command = new Dex();
        byte x;
        for (int i = 512; i > 0; i--) {
            x = cpu.getX();
            assertEquals(i & 0xFF, cpu.getX() & 0xFF);
            final int cycles = command.run(cpu, new StatusRegister(), new ProgramCounter(), new StackPointer());
            assertEquals(2, cycles);
            assertEquals(--x, cpu.getX());
        }
    }

    @Test
    void testFlagsAndPc() {
        final Ram memory = new RamImpl(256 * 256);
        // set values for PC, will be looked up by CPU during initialization
        memory.set((byte) 0xFC, (byte) 0xFF, (byte) 0x01);
        memory.set((byte) 0xFD, (byte) 0xFF, (byte) 0x00);

        memory.set((byte) 0x01, (byte) 0x00, new Dex().getOpcode()); // 0001 DEX
        memory.set((byte) 0x02, (byte) 0x00, new JmpAbsolute().getOpcode()); // 0002 JMP 01 00
        memory.set((byte) 0x03, (byte) 0x00, (byte) 0x01);
        memory.set((byte) 0x04, (byte) 0x00, (byte) 0x00);

        final Cpu cpu = new Mos6502(memory);

        byte x;
        for (int i = 0; i <= 260; i++) {
            x = cpu.getX();

            assertEquals(new AddressVectorImpl((byte) 0x01, (byte) 0x00), cpu.getCurrentPc());

            // 2 cycles for DEX
            cpu.tick();
            cpu.tick();

            assertEquals(new AddressVectorImpl((byte) 0x02, (byte) 0x00), cpu.getCurrentPc());

            // 3 cycles for absolute JMP
            cpu.tick();
            cpu.tick();
            cpu.tick();

            assertEquals(new AddressVectorImpl((byte) 0x01, (byte) 0x00), cpu.getCurrentPc());

            if (cpu.getX() == 0) {
                assertTrue(cpu.getCurrentStatus().isZeroSet());
            } else {
                assertFalse(cpu.getCurrentStatus().isZeroSet());
            }

            // highest bit is 1 -> negative flag is set
            if ((cpu.getX() & 0x80) == 0x80) {
                assertTrue(cpu.getCurrentStatus().isNegativeSet());
            } else {
                assertFalse(cpu.getCurrentStatus().isNegativeSet());
            }

            assertEquals(--x, cpu.getX());
        }
    }
}