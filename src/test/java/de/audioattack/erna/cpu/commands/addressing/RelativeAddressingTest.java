package de.audioattack.erna.cpu.commands.addressing;

import de.audioattack.erna.cpu.AddressVectorImpl;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.Mos6502;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RelativeAddressingTest {

    @Test
    void getAddressZeroOffset() {

        final Addressing calculator = new RelativeAddressing();
        final Cpu cpu = new Mos6502(new RamImpl(256 * 256));

        cpu.setMemory((byte) 0x01, (byte) 0x00, (byte) 0x00);

        final ProgramCounter programCounter = new ProgramCounter();
        programCounter.set((byte) 0x00, (byte) 0x00);

        final AddressResult address = calculator.getAddress(cpu, programCounter);
        assertEquals(new AddressVectorImpl((byte) 0x02, (byte) 0x00), address.getAddress());
        assertFalse(address.isPageBoundaryCrossed());
    }

    @Test
    void getAddressPositiveOffset() {

        final Addressing calculator = new RelativeAddressing();
        final Cpu cpu = new Mos6502(new RamImpl(256 * 256));

        cpu.setMemory((byte) 0x01, (byte) 0x00, (byte) 0x02);

        final ProgramCounter programCounter = new ProgramCounter();

        final AddressResult address = calculator.getAddress(cpu, programCounter);
        assertEquals(new AddressVectorImpl((byte) 0x04, (byte) 0x00), address.getAddress());
        assertFalse(address.isPageBoundaryCrossed());
    }

    @Test
    void getAddressNegativeOffset() {

        final Addressing calculator = new RelativeAddressing();
        final Cpu cpu = new Mos6502(new RamImpl(256 * 256));

        cpu.setMemory((byte) 0x01, (byte) 0x00, (byte) 0xFE);

        final ProgramCounter programCounter = new ProgramCounter();

        final AddressResult address = calculator.getAddress(cpu, programCounter);
        assertEquals(new AddressVectorImpl((byte) 0x00, (byte) 0x00), address.getAddress());
        assertFalse(address.isPageBoundaryCrossed());
    }

    @Test
    void getAddressPositiveOffsetWithPageBoundaryCrossed() {

        final Addressing calculator = new RelativeAddressing();
        final Cpu cpu = new Mos6502(new RamImpl(256 * 256));

        cpu.setMemory((byte) 0xFD, (byte) 0x00, (byte) 0x04);

        final ProgramCounter programCounter = new ProgramCounter();
        programCounter.set((byte) 0xFC, (byte) 0x00);

        final AddressResult address = calculator.getAddress(cpu, programCounter);
        assertEquals(new AddressVectorImpl((byte) 0x02, (byte) 0x01), address.getAddress());
        assertTrue(address.isPageBoundaryCrossed());
    }

    @Test
    void getAddressNegativeOffsetWithPageBoundaryCrossed() {

        final Addressing calculator = new RelativeAddressing();
        final Cpu cpu = new Mos6502(new RamImpl(256 * 256));

        cpu.setMemory((byte) 0x01, (byte) 0x01, (byte) 0xFC);

        final ProgramCounter programCounter = new ProgramCounter();
        programCounter.set((byte) 0x00, (byte) 0x01);

        final AddressResult address = calculator.getAddress(cpu, programCounter);
        assertEquals(new AddressVectorImpl((byte) 0xFE, (byte) 0x000), address.getAddress());
        assertTrue(address.isPageBoundaryCrossed());
    }
}
