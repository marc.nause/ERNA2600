package de.audioattack.erna.cpu.commands.addressing;

import de.audioattack.erna.cpu.AddressVectorImpl;
import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.Mos6502;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class AbsoluteIndirectAddressingTest {

    @Test
    void getAddressSimple() {

        final Addressing calculator = new AbsoluteIndirectAddressing();
        final Cpu cpu = new Mos6502(new RamImpl(256 * 256));

        cpu.setMemory((byte) 0x01, (byte) 0x00, (byte) 0x05);
        cpu.setMemory((byte) 0x02, (byte) 0x00, (byte) 0x06);

        cpu.setMemory((byte) 0x05, (byte) 0x06, (byte) 0xAB);
        cpu.setMemory((byte) 0x06, (byte) 0x06, (byte) 0xCD);

        final ProgramCounter programCounter = new ProgramCounter();
        programCounter.set((byte) 0x00, (byte) 0x00);

        final AddressResult address = calculator.getAddress(cpu, programCounter);
        assertEquals(new AddressVectorImpl((byte) 0xAB, (byte) 0xCD), address.getAddress());
        assertFalse(address.isPageBoundaryCrossed());
    }

    @Test
    void getAddressPageError() {

        /*
         * The indirect jump instruction does not increment the page address when the indirect pointer crosses a page
         * boundary. JMP ($xxFF) will fetch the address from $xxFF and $xx00.
         *
         * https://www.pagetable.com/c64ref/6502/?tab=3
         */

        final Addressing calculator = new AbsoluteIndirectAddressing();
        final Cpu cpu = new Mos6502(new RamImpl(256 * 256));

        cpu.setMemory((byte) 0x01, (byte) 0x00, (byte) 0xFF);
        cpu.setMemory((byte) 0x02, (byte) 0x00, (byte) 0x06);

        cpu.setMemory((byte) 0xFF, (byte) 0x06, (byte) 0xAB);
        cpu.setMemory((byte) 0x00, (byte) 0x06, (byte) 0xCD);

        final ProgramCounter programCounter = new ProgramCounter();
        programCounter.set((byte) 0x00, (byte) 0x00);

        final AddressResult address = calculator.getAddress(cpu, programCounter);
        assertEquals(new AddressVectorImpl((byte) 0xAB, (byte) 0xCD), address.getAddress());
        assertFalse(address.isPageBoundaryCrossed());
    }
}
