package de.audioattack.erna.cpu.commands.addressing;

import de.audioattack.erna.cpu.AddressVector;
import de.audioattack.erna.cpu.AddressVectorImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddressCalculatorTest {

    @Test
    void calcAddressSimple() {

        final AddressVector address = new AddressVectorImpl((byte) 0xF0, (byte) 0x00);
        final AddressResult addressResult = AddressCalculator.add(address, (byte) 0x0E);

        assertFalse(addressResult.isPageBoundaryCrossed());
        assertEquals((byte) 0x00, addressResult.getAddress().getHi());
        assertEquals((byte) 0xFE, addressResult.getAddress().getLo());
    }

    @Test
    void calcAddressSimpleWithPrimitives() {

        final AddressResult addressResult = AddressCalculator.add((byte) 0xF0, (byte) 0x00, (byte) 0x0E);

        assertFalse(addressResult.isPageBoundaryCrossed());
        assertEquals((byte) 0x00, addressResult.getAddress().getHi());
        assertEquals((byte) 0xFE, addressResult.getAddress().getLo());
    }

    @Test
    void calcAddressPageError() {

        final AddressVector address = new AddressVectorImpl((byte) 0xF0, (byte) 0x00);
        final AddressResult addressResult = AddressCalculator.add(address, (byte) 0x11);

        assertTrue(addressResult.isPageBoundaryCrossed());
        assertEquals((byte) 0x01, addressResult.getAddress().getHi());
        assertEquals((byte) 0x01, addressResult.getAddress().getLo());
    }

    @Test
    void calcAddressPageErrorWithPrimitives() {

        final AddressResult addressResult = AddressCalculator.add((byte) 0xF0, (byte) 0x00, (byte) 0x11);

        assertTrue(addressResult.isPageBoundaryCrossed());
        assertEquals((byte) 0x01, addressResult.getAddress().getHi());
        assertEquals((byte) 0x01, addressResult.getAddress().getLo());
    }

    @Test
    void calcAddressWrapAround() {

        final AddressVector address = new AddressVectorImpl((byte) 0xFF, (byte) 0xFF);
        final AddressResult addressResult = AddressCalculator.add(address, (byte) 0x02);

        assertTrue(addressResult.isPageBoundaryCrossed());
        assertEquals((byte) 0x00, addressResult.getAddress().getHi());
        assertEquals((byte) 0x01, addressResult.getAddress().getLo());
    }

    @Test
    void calcAddressWrapAroundWithPrimitives() {

        final AddressResult addressResult = AddressCalculator.add((byte) 0xFF, (byte) 0xFF, (byte) 0x02);

        assertTrue(addressResult.isPageBoundaryCrossed());
        assertEquals((byte) 0x00, addressResult.getAddress().getHi());
        assertEquals((byte) 0x01, addressResult.getAddress().getLo());
    }

    @Test
    void calcAddressPlusZero() {

        final AddressVector address = new AddressVectorImpl((byte) 0xFF, (byte) 0xFF);
        final AddressResult addressResult = AddressCalculator.add(address, (byte) 0x00);

        assertFalse(addressResult.isPageBoundaryCrossed());
        assertEquals((byte) 0xFF, addressResult.getAddress().getHi());
        assertEquals((byte) 0xFF, addressResult.getAddress().getLo());
    }

    @Test
    void calcAddressPlusZeroWithPrimitives() {

        final AddressResult addressResult = AddressCalculator.add((byte) 0xFF, (byte) 0xFF, (byte) 0x00);

        assertFalse(addressResult.isPageBoundaryCrossed());
        assertEquals((byte) 0xFF, addressResult.getAddress().getHi());
        assertEquals((byte) 0xFF, addressResult.getAddress().getLo());
    }

    @Test
    void calcAddressSignedPlusZero() {

        final AddressVector address = new AddressVectorImpl((byte) 0xFF, (byte) 0xFF);
        final AddressResult addressResult = AddressCalculator.addSigned(address, (byte) 0x00);

        assertFalse(addressResult.isPageBoundaryCrossed());
        assertEquals((byte) 0xFF, addressResult.getAddress().getHi());
        assertEquals((byte) 0xFF, addressResult.getAddress().getLo());
    }

    @Test
    void calcAddressPSignedlusZeroWithPrimitives() {

        final AddressResult addressResult = AddressCalculator.addSigned((byte) 0xFF, (byte) 0xFF, (byte) 0x00);

        assertFalse(addressResult.isPageBoundaryCrossed());
        assertEquals((byte) 0xFF, addressResult.getAddress().getHi());
        assertEquals((byte) 0xFF, addressResult.getAddress().getLo());
    }

    @Test
    void calcAddressSignedPlusPositive() {

        final AddressVector address = new AddressVectorImpl((byte) 0x00, (byte) 0x01);
        final AddressResult addressResult = AddressCalculator.addSigned(address, (byte) 0x02);

        assertFalse(addressResult.isPageBoundaryCrossed());
        assertEquals((byte) 0x01, addressResult.getAddress().getHi());
        assertEquals((byte) 0x02, addressResult.getAddress().getLo());
    }

    @Test
    void calcAddressSignedPlusNegative() {

        final AddressVector address = new AddressVectorImpl((byte) 0xFF, (byte) 0x01);
        final AddressResult addressResult = AddressCalculator.addSigned(address, (byte) 0x80);

        assertFalse(addressResult.isPageBoundaryCrossed());
        assertEquals((byte) 0x01, addressResult.getAddress().getHi());
        assertEquals((byte) 0x7F, addressResult.getAddress().getLo());
    }

    @Test
    void calcAddressSignedPlusPositiveWithPageBoundaryCrossed() {

        final AddressVector address = new AddressVectorImpl((byte) 0xFF, (byte) 0x00);
        final AddressResult addressResult = AddressCalculator.addSigned(address, (byte) 0x01);
        assertTrue(addressResult.isPageBoundaryCrossed());
        assertEquals((byte) 0x01, addressResult.getAddress().getHi());
        assertEquals((byte) 0x00, addressResult.getAddress().getLo());
    }

    @Test
    void calcAddressSignedPlusNegativeWithPageBoundaryCrossed() {

        final AddressVector address = new AddressVectorImpl((byte) 0x01, (byte) 0x01);
        final AddressResult addressResult = AddressCalculator.addSigned(address, (byte) 0xFE);

        assertTrue(addressResult.isPageBoundaryCrossed());
        assertEquals((byte) 0x00, addressResult.getAddress().getHi());
        assertEquals((byte) 0xFF, addressResult.getAddress().getLo());
    }

}
