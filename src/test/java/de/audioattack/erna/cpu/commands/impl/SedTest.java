package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.Mos6502;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.Command;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SedTest {

    @Test
    void run() {
        final Cpu cpu = new Mos6502(new RamImpl(256 * 256));
        final StatusRegister statusRegister = new StatusRegister();
        final Command command = new Sed();
        assertFalse(statusRegister.isDecimalSet());
        final int cycles = command.run(cpu, statusRegister, new ProgramCounter(), new StackPointer());
        assertTrue(statusRegister.isDecimalSet());
        assertEquals(2, cycles);
    }

    // TODO add test for flags and PC
}