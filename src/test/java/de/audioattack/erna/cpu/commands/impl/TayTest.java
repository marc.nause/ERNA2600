package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.Mos6502;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.Command;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TayTest {

    @Test
    void run() {
        final Cpu cpu = new Mos6502(new RamImpl(256 * 256));
        final Command command = new Tay();
        assertEquals((byte) 0x00, cpu.getA());
        assertEquals((byte) 0x00, cpu.getY());
        cpu.setA((byte) 0xFF);
        assertEquals((byte) 0xFF, cpu.getA());
        assertEquals((byte) 0x00, cpu.getY());
        final int cycles = command.run(cpu, new StatusRegister(), new ProgramCounter(), new StackPointer());
        assertEquals((byte) 0xFF, cpu.getA());
        assertEquals((byte) 0xFF, cpu.getY());
        assertEquals(2, cycles);
    }

    // TODO add test for flags and PC
}