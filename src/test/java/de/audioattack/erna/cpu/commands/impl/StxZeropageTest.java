package de.audioattack.erna.cpu.commands.impl;

import de.audioattack.erna.cpu.Cpu;
import de.audioattack.erna.cpu.Mos6502;
import de.audioattack.erna.cpu.ProgramCounter;
import de.audioattack.erna.cpu.StackPointer;
import de.audioattack.erna.cpu.StatusRegister;
import de.audioattack.erna.cpu.commands.Command;
import de.audioattack.erna.memory.Ram;
import de.audioattack.erna.memory.RamImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StxZeropageTest {

    @Test
    void run() {
        final Command command = new StxZeropage();

        final Ram memory = new RamImpl(256 * 256);

        memory.set((byte) 0x00, (byte) 0x00, command.getOpcode()); // 0001 STX
        memory.set((byte) 0x01, (byte) 0x00, (byte) 0x02);
        memory.set((byte) 0x02, (byte) 0x00, (byte) 0xBA);

        final Cpu cpu = new Mos6502(memory);
        cpu.setX((byte) 0xAB);

        assertEquals((byte) 0xBA, cpu.getMemory((byte) 0x02, (byte) 0x00));
        final int cycles = command.run(cpu, new StatusRegister(), new ProgramCounter(), new StackPointer());
        assertEquals((byte) 0xAB, cpu.getMemory((byte) 0x02, (byte) 0x00));
        assertEquals(3, cycles);
    }

    // TODO add test for flags and PC
}