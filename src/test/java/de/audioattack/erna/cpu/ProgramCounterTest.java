package de.audioattack.erna.cpu;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProgramCounterTest {

    @Test
    void testSet() {

        final ProgramCounter pc = new ProgramCounter();
        pc.set(new AddressVectorImpl((byte) 3, (byte) 5));

        assertEquals(3, pc.getLo());
        assertEquals(5, pc.getHi());
    }

    @Test
    void testIncremet() {

        final ProgramCounter pc = new ProgramCounter();
        pc.set(new AddressVectorImpl((byte) 255, (byte) 0));

        pc.increment();
        assertEquals(0, pc.getLo());
        assertEquals(1, pc.getHi());
    }

    @Test
    void testIncremetLoop() {

        final ProgramCounter pc = new ProgramCounter();
        pc.set(new AddressVectorImpl((byte) 0, (byte) 0));

        int i = 0;

        while (i < 255 * 255) {

            pc.increment();
            i++;
            assertEquals(i, (pc.getLo() & 0xFF) + ((pc.getHi() & 0xFF) << 8));
        }
    }

    @Test
    void testDecremet() {

        final ProgramCounter pc = new ProgramCounter();
        pc.set(new AddressVectorImpl((byte) 0, (byte) 1));

        pc.decrement();
        assertEquals(255, pc.getLo() & 0xFF);
        assertEquals(0, pc.getHi() & 0xFF);
    }

    @Test
    void testDecremetLoop() {

        final ProgramCounter pc = new ProgramCounter();
        pc.set(new AddressVectorImpl((byte) 255, (byte) 255));

        int i = 256 * 256 - 1;

        while (i > 0) {

            pc.decrement();
            assertEquals(--i, (pc.getLo() & 0xFF) + ((pc.getHi() & 0xFF) << 8));
        }
    }

}
